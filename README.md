 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 How to run the application:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 java -jar <<application-name>>.jar
 
 This will start the internal tomcat that is shipped with sprint boot. Now REST ready to be consumed.
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Consuming the REST API:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Each entity has rest end points in the format like the one mentioned below.
 ########################################################
 Operation          REST end point        HTTP
 ########################################################
 Create           <<entity>>/create 			POST
 Update           <<entity>>/update 			PUT
 Delete			<<entity>>/delete       	DELETE
 Get all   		<<entity>>/fetchAllRecords  GET
 (Non paginated)
 Get all   		<<entity>>/getAllRecords   GET
 (Paginated)
 ########################################################
 
 Here entity is the domain object. i.e. Java classes in domain package
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Configuration Management:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Out of box development, QA, UAT, production profiles are supported.
 
 These profiles are configured in application.properties (for dev), application-qa.properties, application-uat.properties, application-production.properties
 
 If you want a property specific to a given environment, then it should be part of its respective file.
 
 
 Activating profile:
 ###################
 Pass the profile as argument when you run the application to activate a specific profile.
 Right profile is picked based on value of -Dspring.profiles.active
 
 Ex: 
 
 java -jar -Dspring.profiles.active=production sudrania-0.0.1-SNAPSHOT.jar 
 -- This would pick application-production.properties
 
 java -jar -Dspring.profiles.active=uat sudrania-0.0.1-SNAPSHOT.jar 
 -- This would pick application-uat.properties
 
 If nothing is passed, then application.properties is picked. Which should be used for development purpose.
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Logging:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Logging is pre-configured. All the logs in the dev, qa and uat environments go to logs/application.log
 Production logs go to logs/production/application.log
 
 Customize logs to user needs. For example if you wish to add rotation policy to the logging system, do so.
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Mongo DB:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 By default application will connect to free cloud based mongo database from mlab.com. This is configured in application.properties.
 Check the attribute spring.data.mongodb.uri. If you wish to change the uri format to host, port, user and password kind of thing. This is the place to do so.
 
 Configure the same in other property files to make the application connect to different databases in different environments.
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Caching:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  Caching is pre-configured in the application. Redis is used for caching out of the box. Application makes use of cloud based redis vendor redislabs.com.
  Cloud based service that application makes use of is only for development purposes. Check spring.redis.host, spring.redis.port and spring.redis.password in the application*.properties files.
  
  @EnableCaching annotation on <code>Application.java</code> does the magic of making caching available.
  @Cacheable annotations in the service layer are commented. Start un-commenting and use them when you wish to cache objects.
  
  For effective use of caching related annotations one needs to understand spring expression language.
  
  Refer to below link to understand spring expression language:
  http://www.baeldung.com/spring-expression-language

 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Validation:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 Validation layer is pre-configured in the application. When a controller gets a /create or a /update call validator configured in initBinder method gets triggered.
 Write your validations in validate() method in that class.
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Internationalization:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 Resource bundles will be available under src/main/resources/i18n directory. Any message/string that will be seen by client should be configured here.
 Check localeResolver() and messageSource() methods in Application.java configures the resource bundles.
 
 @Autowirede MessageSource objects makes the resource bundles available to the application classes. MessageSource is made available to the Validation and Services layers out of the box.  
 
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Business logic:
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 Business logic layer is hooked into the service layer during the CRUD operations. Implement the business logic in the perform() methods of the business object.

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Things to look into before deploying to production:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready-sensitive-endpoints
