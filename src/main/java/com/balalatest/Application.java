package com.balalatest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import java.util.Locale;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.balalatest.domain.Account;
import com.balalatest.repository.AccountRepository;
import com.balalatest.domain.core.UserCategory;
import javax.annotation.PostConstruct;

/**
 * <pre>
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * How to run the application:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * java -jar <<application-name>>.jar
 * 
 * This will start the internal tomcat that is shipped with sprint boot. Now REST ready to be consumed.
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Consuming the REST API:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Each entity has rest end points in the format like the one mentioned below.
 * ########################################################
 * Operation          REST end point        HTTP
 * ########################################################
 * Create           <<entity>>/create 			POST
 * Update           <<entity>>/update 			PUT
 * Delete			<<entity>>/delete       	DELETE
 * Get all   		<<entity>>/fetchAllRecords  GET
 * (Non paginated)
 * Get all   		<<entity>>/getAllRecords   GET
 * (Paginated)
 * ########################################################
 * 
 * Here entity is the domain object. i.e. Java classes in domain package
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Configuration Management:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Out of box development, QA, UAT, production profiles are supported.
 * 
 * These profiles are configured in application.properties (for dev), application-qa.properties, application-uat.properties, application-production.properties
 * 
 * If you want a property specific to a given environment, then it should be part of its respective file.
 * 
 * 
 * Activating profile:
 * ###################
 * Pass the profile as argument when you run the application to activate a specific profile.
 * Right profile is picked based on value of -Dspring.profiles.active
 * 
 * Ex: 
 * 
 * java -jar -Dspring.profiles.active=production sudrania-0.0.1-SNAPSHOT.jar 
 * -- This would pick application-production.properties
 * 
 * java -jar -Dspring.profiles.active=uat sudrania-0.0.1-SNAPSHOT.jar 
 * -- This would pick application-uat.properties
 * 
 * If nothing is passed, then application.properties is picked. Which should be used for development purpose.
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Logging:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Logging is pre-configured. All the logs in the dev, qa and uat environments go to logs/application.log
 * Production logs go to logs/production/application.log
 * 
 * Customize logs to user needs. For example if you wish to add rotation policy to the logging system, do so.
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Mongo DB:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * By default application will connect to free cloud based mongo database from mlab.com. This is configured in application.properties.
 * Check the attribute spring.data.mongodb.uri. If you wish to change the uri format to host, port, user and password kind of thing. This is the place to do so.
 * 
 * Configure the same in other property files to make the application connect to different databases in different environments.
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Caching:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *  Caching is pre-configured in the application. Redis is used for caching out of the box. Application makes use of cloud based redis vendor redislabs.com.
 *  Cloud based service that application makes use of is only for development purposes. Check spring.redis.host, spring.redis.port and spring.redis.password in the application*.properties files.
 *  
 *  @EnableCaching annotation on <code>Application.java</code> does the magic of making caching available.
 *  @Cacheable annotations in the service layer are commented. Start un-commenting and use them when you wish to cache objects.
 * </pre>
 * <pre>
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Validation:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 * Validation layer is pre-configured in the application. When a controller gets a /create or a /update call validator configured in initBinder method gets triggered.
 * Write your validations in validate() method in that class.
 *
 * Required, RequiredWhen and Unique annotations are validated out of box.
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Internationalization:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 * Resource bundles will be available under src/main/resources/i18n directory. Any message/string that will be seen by client should be configured here.
 * Check localeResolver() and messageSource() methods in Application.java configures the resource bundles.
 * 
 * @Autowirede MessageSource objects makes the resource bundles available to the application classes. MessageSource is made available to the Validation and Services layers out of the box.  
 * 
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * Business logic:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 * Business logic layer is hooked into the service layer during the CRUD operations. Implement the business logic in the perform() methods of the business object.
 *
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * User Authentication:
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 * User authentication is pre-built into the application. There is a user with name admin and password admin created for default. Pass these credentials while doing the REST calls.
 * </pre>
 * 
 * @author Shris Infotech
 */
//@EnableWebSecurity
@EnableCaching
@EnableMongoAuditing
@SpringBootApplication
public class Application {
	
	@Autowired
	AccountRepository accountRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@PostConstruct
	public void initializeWithDefaults() {
		final Account account = accountRepository.findByUsername("admin");
		if (account == null) {
			accountRepository.save(new Account("admin", new BCryptPasswordEncoder().encode("admin"), UserCategory.Admin));
		}
	}
	
	/**
	 * Setting up locale resolver, and making English US as default locale for the application.
	 * Unless locale is specified messages from messages_en.properties are used.
	 * 
	 * @return
	 */
	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US); // Set default Locale as US
		return slr;
	}
	
	/**
	 * Setting location of resource bundles to i18n/messages
	 * 
	 * @return
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames("i18n/messages"); // name of the resource bundle
		source.setUseCodeAsDefaultMessage(true);
		return source;
	}
}


