package com.balalatest.businessobject;

import java.util.Collection;
import java.util.UUID;
import com.balalatest.domain.BestExecOrders;

/**
 * Performs business logic for BestExecOrders.
 * 
 * 
 * @see Context
 * @see BestExecOrders
 * @see BestExecOrdersBusinessService#create(BestExecOrders)
 * @see BestExecOrdersBusinessService#update(BestExecOrders)
 * @see BestExecOrdersBusinessService#delete(BestExecOrders)
 *
 * @author Shris Infotech
 */
public class BestExecOrdersBusinessLogic {
	
	/**
	 * Implements business logic for {@link BestExecOrders}
	 * 
	 * @param entity
	 * @param context
	 */	
	public void perform(final BestExecOrders bestExecOrders, final Context context) {
		// Perform your business logic here.
		// Ex: If a and b are part of the incoming object and if we have to do a a+ b and assign to c. it has to be done here.
		if(Context.CREATE.equals(context)) {
			// Logic that needs to be performed during creation of the object
			final String textId = UUID.randomUUID().toString();
			bestExecOrders.setId(textId);
		} else if(Context.UPDATE.equals(context)) {
			// Logic that needs to be performed during update of the object
		} else if(Context.DELETE.equals(context)) {
			// Logic that needs to be performed during delete of the object
		} else if(Context.GET.equals(context)) {
			// Logic that needs to be performed during get operation
		} else if(Context.FIND.equals(context)) {
			// Logic that needs to be performed during the find operation
		}
	}
	
	/**
	 * Implements business logic for a collection of {@link Ticker} objects
	 * 
	 * @param entityCollection
	 * @param context
	 */
	public void perform(final Collection<BestExecOrders> bestExecOrdersCollection, final Context context) {
		if(bestExecOrdersCollection != null && !bestExecOrdersCollection.isEmpty()) {
			for (final BestExecOrders entity : bestExecOrdersCollection) {
				perform(entity, context);
			}
		}
	}
	
}