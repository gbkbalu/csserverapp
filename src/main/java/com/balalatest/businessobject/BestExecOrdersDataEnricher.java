package com.balalatest.businessobject;

import java.util.Collection;
import org.springframework.data.domain.Page;
import com.balalatest.domain.BestExecOrders;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see BestExecOrdersDataService
 * @author Shris Infotech
 * 
 */
public class BestExecOrdersDataEnricher {
	
	/**
	 * Enriches bestExecOrderss domain object.
	 * 
	 * @param bestExecOrders
	 */
	public void enrich(final BestExecOrders bestExecOrders) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of bestExecOrderss domain objects.
	 * 
	 * @param bestExecOrderss
	 */
	public void enrich(final Collection<BestExecOrders> bestExecOrderss) {
		if(bestExecOrderss == null || bestExecOrderss.isEmpty()) {
			return;
		}
		for (BestExecOrders bestExecOrders : bestExecOrderss) {
			enrich(bestExecOrders);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<BestExecOrders> entities) {
		if(entities == null) {
			return;
		}
		
		for (BestExecOrders bestExecOrders : entities) {
			enrich(bestExecOrders);
		}
	}
	
}
