package com.balalatest.businessobject;

/**
 * An enumeration of possible values used when a business object's perform method is invoked.
 * 
 * @author Shris Infotech
 *
 */
public enum Context {
	CREATE, UPDATE, DELETE, GET, FIND
}
