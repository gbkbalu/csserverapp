package com.balalatest.businessobject;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.balalatest.domain.Enterprise;
import com.balalatest.service.EnterpriseDataService;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see EnterpriseDataService
 * @author Shris Infotech
 * 
 */
public class EnterpriseDataEnricher {
	
	/**
	 * Enriches enterprises domain object.
	 * 
	 * @param enterprise
	 */
	public void enrich(final Enterprise enterprise) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of enterprises domain objects.
	 * 
	 * @param enterprises
	 */
	public void enrich(final Collection<Enterprise> enterprises) {
		if(enterprises == null || enterprises.isEmpty()) {
			return;
		}
		for (Enterprise enterprise : enterprises) {
			enrich(enterprise);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<Enterprise> entities) {
		if(entities == null) {
			return;
		}
		
		for (Enterprise enterprise : entities) {
			enrich(enterprise);
		}
	}
	
}
