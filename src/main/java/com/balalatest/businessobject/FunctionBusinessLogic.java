package com.balalatest.businessobject;

import com.balalatest.domain.Function;
import java.util.UUID;
import java.util.Collection;

/**
 * Performs business logic for Function.
 * 
 * 
 * @see Context
 * @see Function
 * @see FunctionBusinessService#create(Function)
 * @see FunctionBusinessService#update(Function)
 * @see FunctionBusinessService#delete(Function)
 *
 * @author Shris Infotech
 */
public class FunctionBusinessLogic {
	
	/**
	 * Implements business logic for {@link Function}
	 * 
	 * @param entity
	 * @param context
	 */	
	public void perform(final Function function, final Context context) {
		// Perform your business logic here.
		// Ex: If a and b are part of the incoming object and if we have to do a a+ b and assign to c. it has to be done here.
		if(Context.CREATE.equals(context)) {
			// Logic that needs to be performed during creation of the object
		} else if(Context.UPDATE.equals(context)) {
			// Logic that needs to be performed during update of the object
		} else if(Context.DELETE.equals(context)) {
			// Logic that needs to be performed during delete of the object
		} else if(Context.GET.equals(context)) {
			// Logic that needs to be performed during get operation
		} else if(Context.FIND.equals(context)) {
			// Logic that needs to be performed during the find operation
		}
	}
	
	/**
	 * Implements business logic for a collection of {@link Ticker} objects
	 * 
	 * @param entityCollection
	 * @param context
	 */
	public void perform(final Collection<Function> functionCollection, final Context context) {
		if(functionCollection != null && !functionCollection.isEmpty()) {
			for (final Function entity : functionCollection) {
				perform(entity, context);
			}
		}
	}
	
}