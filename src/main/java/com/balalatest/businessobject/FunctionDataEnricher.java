package com.balalatest.businessobject;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.balalatest.domain.Function;
import com.balalatest.service.FunctionDataService;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see FunctionDataService
 * @author Shris Infotech
 * 
 */
public class FunctionDataEnricher {
	
	/**
	 * Enriches functions domain object.
	 * 
	 * @param function
	 */
	public void enrich(final Function function) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of functions domain objects.
	 * 
	 * @param functions
	 */
	public void enrich(final Collection<Function> functions) {
		if(functions == null || functions.isEmpty()) {
			return;
		}
		for (Function function : functions) {
			enrich(function);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<Function> entities) {
		if(entities == null) {
			return;
		}
		
		for (Function function : entities) {
			enrich(function);
		}
	}
	
}
