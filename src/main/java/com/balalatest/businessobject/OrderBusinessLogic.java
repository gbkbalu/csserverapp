package com.balalatest.businessobject;

import java.util.Collection;
import java.util.UUID;
import com.balalatest.domain.Order;
import com.balalatest.service.OrderBusinessService;

/**
 * Performs business logic for Order.
 * 
 * 
 * @see Context
 * @see Order
 * @see OrderBusinessService#create(Order)
 * @see OrderBusinessService#update(Order)
 * @see OrderBusinessService#delete(Order)
 *
 * @author Shris Infotech
 */
public class OrderBusinessLogic {
	
	/**
	 * Implements business logic for {@link Order}
	 * 
	 * @param entity
	 * @param context
	 */	
	public void perform(final Order order, final Context context) {
		// Perform your business logic here.
		// Ex: If a and b are part of the incoming object and if we have to do a a+ b and assign to c. it has to be done here.
		if(Context.CREATE.equals(context)) {
			// Logic that needs to be performed during creation of the object
			final String textId = UUID.randomUUID().toString();
			order.setId(textId);
		} else if(Context.UPDATE.equals(context)) {
			// Logic that needs to be performed during update of the object
		} else if(Context.DELETE.equals(context)) {
			// Logic that needs to be performed during delete of the object
		} else if(Context.GET.equals(context)) {
			// Logic that needs to be performed during get operation
		} else if(Context.FIND.equals(context)) {
			// Logic that needs to be performed during the find operation
		}
	}
	
	/**
	 * Implements business logic for a collection of {@link Ticker} objects
	 * 
	 * @param entityCollection
	 * @param context
	 */
	public void perform(final Collection<Order> orderCollection, final Context context) {
		if(orderCollection != null && !orderCollection.isEmpty()) {
			for (final Order entity : orderCollection) {
				perform(entity, context);
			}
		}
	}
	
}