package com.balalatest.businessobject;

import java.util.Collection;
import org.springframework.data.domain.Page;
import com.balalatest.domain.Order;
import com.balalatest.service.OrderDataService;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see OrderDataService
 * @author Shris Infotech
 * 
 */
public class OrderDataEnricher {
	
	/**
	 * Enriches orders domain object.
	 * 
	 * @param order
	 */
	public void enrich(final Order order) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of orders domain objects.
	 * 
	 * @param orders
	 */
	public void enrich(final Collection<Order> orders) {
		if(orders == null || orders.isEmpty()) {
			return;
		}
		for (Order order : orders) {
			enrich(order);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<Order> entities) {
		if(entities == null) {
			return;
		}
		
		for (Order order : entities) {
			enrich(order);
		}
	}
	
}
