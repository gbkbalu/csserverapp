package com.balalatest.businessobject;

import com.balalatest.domain.Role;
import java.util.UUID;
import java.util.Collection;

/**
 * Performs business logic for Role.
 * 
 * 
 * @see Context
 * @see Role
 * @see RoleBusinessService#create(Role)
 * @see RoleBusinessService#update(Role)
 * @see RoleBusinessService#delete(Role)
 *
 * @author Shris Infotech
 */
public class RoleBusinessLogic {
	
	/**
	 * Implements business logic for {@link Role}
	 * 
	 * @param entity
	 * @param context
	 */	
	public void perform(final Role role, final Context context) {
		// Perform your business logic here.
		// Ex: If a and b are part of the incoming object and if we have to do a a+ b and assign to c. it has to be done here.
		if(Context.CREATE.equals(context)) {
			// Logic that needs to be performed during creation of the object
		} else if(Context.UPDATE.equals(context)) {
			// Logic that needs to be performed during update of the object
		} else if(Context.DELETE.equals(context)) {
			// Logic that needs to be performed during delete of the object
		} else if(Context.GET.equals(context)) {
			// Logic that needs to be performed during get operation
		} else if(Context.FIND.equals(context)) {
			// Logic that needs to be performed during the find operation
		}
	}
	
	/**
	 * Implements business logic for a collection of {@link Ticker} objects
	 * 
	 * @param entityCollection
	 * @param context
	 */
	public void perform(final Collection<Role> roleCollection, final Context context) {
		if(roleCollection != null && !roleCollection.isEmpty()) {
			for (final Role entity : roleCollection) {
				perform(entity, context);
			}
		}
	}
	
}