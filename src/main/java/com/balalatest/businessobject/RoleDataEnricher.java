package com.balalatest.businessobject;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.balalatest.domain.Role;
import com.balalatest.service.RoleDataService;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see RoleDataService
 * @author Shris Infotech
 * 
 */
public class RoleDataEnricher {
	
	/**
	 * Enriches roles domain object.
	 * 
	 * @param role
	 */
	public void enrich(final Role role) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of roles domain objects.
	 * 
	 * @param roles
	 */
	public void enrich(final Collection<Role> roles) {
		if(roles == null || roles.isEmpty()) {
			return;
		}
		for (Role role : roles) {
			enrich(role);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<Role> entities) {
		if(entities == null) {
			return;
		}
		
		for (Role role : entities) {
			enrich(role);
		}
	}
	
}
