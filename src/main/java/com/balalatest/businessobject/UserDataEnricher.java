package com.balalatest.businessobject;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.balalatest.domain.User;
import com.balalatest.service.UserDataService;

/**
 * Enriches data before returning domain objects of type Doctor to the business service layer.
 * 
 * Examples for data enrichment:
 * -----------------------------
 * 1. Conversion of date format
 * 2. Currency conversions
 * 3. Populating salutations based on Gender
 * 4. Converting case of a string (Like upper to lower - lower to upper) etc...
 *  
 * @see UserDataService
 * @author Shris Infotech
 * 
 */
public class UserDataEnricher {
	
	/**
	 * Enriches users domain object.
	 * 
	 * @param user
	 */
	public void enrich(final User user) {
		// TO DO: Enrich the data
		// 
	}
	
	/**
	 * Enriches collection of users domain objects.
	 * 
	 * @param users
	 */
	public void enrich(final Collection<User> users) {
		if(users == null || users.isEmpty()) {
			return;
		}
		for (User user : users) {
			enrich(user);
		}
	}
	
	/**
	 * Enriches Page set of doctor domain objects.
	 * 
	 * @param entities
	 */
	public void enrich(Page<User> entities) {
		if(entities == null) {
			return;
		}
		
		for (User user : entities) {
			enrich(user);
		}
	}
	
}
