package com.balalatest.common;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.balalatest.domain.core.BaseDomain;

/**
 * Common utility methods.
 * 
 * @author Shris Infotech.
 *
 */
public class CommonUtil {
	
	/**
	 * Returns list of column names who value has changed.
	 * 
	 * @param oldEntity
	 * @param newEntity
	 * @return a Map containing name of the field as key and an object array where object[0] is the old value and object[1] is the new value.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object[]> changedColumns(BaseDomain oldEntity, BaseDomain newEntity) {
		
		Field[] fields = oldEntity.getClass().getDeclaredFields();
		if(fields == null || fields.length == 0) {
			return Collections.EMPTY_MAP;
		}
		
		Map<String, Object[]> changedColumnValues = new HashMap<String, Object[]>();
		Object oldFiledValue, newFiledValue;
		
		for (Field field : fields) {
			
			try {
				oldFiledValue = oldEntity.getProperty(field.getName());
			    newFiledValue = newEntity.getProperty(field.getName());
			    
			    if(oldFiledValue == newFiledValue) {
					continue;
				}
				
				if(oldFiledValue!= null && !oldFiledValue.equals(newFiledValue)) {
					changedColumnValues.put(field.getName(), new Object[]{oldFiledValue,newFiledValue});
				}
				
			} catch (Exception e) {
				// Do nothing
				e.printStackTrace();
			}
			
		}
		
		return changedColumnValues;
	}
}
