package com.balalatest.common;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Contains utility methods to create object to and from JSON.
 * 
 * @author Shris Infotech
 *
 */
public class JSONUtil {
	
	/**
	 * Creates a POJO from a JSON string.
	 * 
	 * @param entityClass Class that needs to be created from incoming JSON
	 * @param jsonObject JSON string that needs to used to create entityClass
	 * @return
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws IOException
	 */
	public static <T> T getEntityFromJson(final Class<T> entityClass, final String jsonObject)
			throws JsonMappingException, JsonParseException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		T object = mapper.readValue(jsonObject, entityClass);
		return object;
	}
	
	/**
	 * CreatesJSON representation of a Java object.
	 * 
	 * @param object
	 * @return
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws IOException
	 */
	public static <T> String getJsonFromEntity(T object) throws JsonMappingException, JsonParseException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}
}	