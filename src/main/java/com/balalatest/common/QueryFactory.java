package com.balalatest.common;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.balalatest.domain.core.annotation.EnterpriseSpecific;
import com.balalatest.domain.core.annotation.SoftDelete;

/**
 * Factory class to create Query objects to query mongo database.
 * 
 * 
 * @author Shris Infotech.
 *
 */
public class QueryFactory {
	
	/**
	 * Creates query object and pre-populates it with certain defaults based on the domain object that needs to be queried.
	 * Returns a new  and empty query object when classToQuery is null.
	 * Ex: createQuery(Emp.class)
	 * 
	 * @param classToQuery 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Query createQuery(Class classToQuery) {
		
		Query query = createSimpleQuery();
		
		// Return empty query.
		if(classToQuery == null) {
			return query;
		}
		
		// Check if the class to be queried of soft delete type.
		SoftDelete softDelete = (SoftDelete) classToQuery.getAnnotation(SoftDelete.class);
		// Check if the class to be queried is enterprise specific.
		EnterpriseSpecific enterpriseSpecific = (EnterpriseSpecific) classToQuery.getAnnotation(EnterpriseSpecific.class);
		boolean softDeleteAdded = false;
		Criteria criteria = null;
		
		// If the class is soft delete type, add deleteFlag False as criteria.
		if(softDelete != null) {
			criteria = Criteria.where("deleteFlag").is(Boolean.FALSE);
			query.addCriteria(criteria);
			softDeleteAdded = true;
		}
		
		// If the class is enterprise specific, add enterprise id to the query criteria. 
		if(enterpriseSpecific != null) {
			// To Do: Populate enterprise id from currently logged in user object
			if(softDeleteAdded) {
				criteria.and("enterpriseId").is("");
			} else {
				criteria = Criteria.where("enterpriseId").is("");
				query.addCriteria(criteria);
			}
		}
		
		return query;
	}
	
	/**
	 * factory method for creating Query object.
	 * @return
	 */
	public static Query createSimpleQuery() {
		return new Query();
	}
}
