package com.balalatest.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.domain.PageRequest;

import org.springframework.stereotype.Service;

import com.balalatest.domain.core.*;
import com.balalatest.domain.core.annotation.*;
import com.balalatest.constants.AppConstants;


@Service
public class QueryUtil {

	@Autowired
	MongoTemplate template;
	
	public  Query createQueryFromJson(JSONObject jsonObj) {
		
		if(jsonObj == null) {
			return null;
		}
		
		Query query = new Query();
		Criteria criteria = null;
		int pageNum = 0;
		int pageSize = 0;
		
		Set keys = jsonObj.keySet();
				
		if(keys == null || keys.isEmpty()) {
			return query;
		}
		
		Object[] keysArray = keys.toArray();
		String key = null;
		Object value = null;
		int iteLen = 0;
		for (int index=0;index<keys.size();index++) {
			key = keysArray[index].toString();
			value = jsonObj.get(key);
			
			if (key.equalsIgnoreCase("page")) 
			{
				pageNum = Integer.parseInt(value.toString());
			}else if (key.equalsIgnoreCase("pageSize")) 
			{
				pageSize = Integer.parseInt(value.toString());
			}else
			{
				if(iteLen == 0) {
					iteLen++;
					criteria = Criteria.where(key).regex((String)value);
					query.addCriteria(criteria);
				} else {
					criteria.and(key).regex(value == null ? (String)value : value.toString());
				}
			}
		}
		
		final PageRequest pageRequest = new PageRequest(pageNum, getPageSize(pageSize));

		query.with(pageRequest);
		return query;
	}
	
	public Map<UniqueFields, Query> createQueryFromUniqueAnnotation(BaseDomain baseDomain,
			UniqueFields[] uniqueFileds) {

		if (uniqueFileds == null) {
			return null;
		}

		Criteria criteria = null;
		String key = null;
		Object value = null;

		Map<UniqueFields, Query> queriesMap = new HashMap<UniqueFields, Query>();
		boolean skipQuery = false;
		
		for (UniqueFields uniqueFields : uniqueFileds) {
			skipQuery = false;
			Query query = new Query();
			// Get the fields
			String[] fileds = uniqueFields.fields();
			if (fileds == null) {
				continue;
			}
			
			
			for (int index = 0; index < fileds.length; index++) {
				key = fileds[index];
				try {
					value = baseDomain.getProperty(key);
					// if value for a field is empty do not validate it. So skip retuning that query.
					if (value == null) {
						skipQuery = true;
						break;
					}
					if (index == 0) {
						criteria = Criteria.where(key).is(value);
						query.addCriteria(criteria);
					} else {
						criteria.and(key).is(value);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			if(!skipQuery) {
				queriesMap.put(uniqueFields, query);
			}
		}

		return queriesMap;
	}
	
	/**
	 * Loads an domain object from data base by Id field and selects only Id filed and name field property
	 * 
	 * @param domainObject
	 * @return
	 */
	public BaseDomain loadByIdField(KeyValueFieldMarker keyValueFiledMarker) {
		Query query = QueryFactory.createSimpleQuery();
		
		query.fields().include(keyValueFiledMarker.idField());
		query.fields().include(keyValueFiledMarker.nameField());
		
		query.addCriteria(Criteria.where(keyValueFiledMarker.idField()).is(keyValueFiledMarker.getId()));
		return (BaseDomain) template.findOne(query, keyValueFiledMarker.getClass());
	}
	
	/**
	 * Loads an domain object from data base by name field and selects only Id filed and name field property
	 * 
	 * @param domainObject
	 * @return
	 */
	public BaseDomain loadByNameField(KeyValueFieldMarker keyValueFiledMarker) {
		Query query = QueryFactory.createSimpleQuery();
		
		query.fields().include(keyValueFiledMarker.idField());
		query.fields().include(keyValueFiledMarker.nameField());
		
		query.addCriteria(Criteria.where(keyValueFiledMarker.idField()).is(keyValueFiledMarker.getName()));
		return (BaseDomain) template.findOne(query, keyValueFiledMarker.getClass());
	}
	
	/**
	 * 
	 * @param pageSize
	 * @return default page if input pageSize <10
	 */
	public int getPageSize(int pageSize) {
		if(pageSize<=0)
		{
			pageSize = AppConstants.PAGE_LIMIT;
		}
		return pageSize;
	}
	
	/**
	 * 
	 * @param id
	 * @return a responseObject
	 */
	public ResponseObject getResponseObject(String id) {
		ResponseObject responseObject = new ResponseObject();
		responseObject.setId(id);
		return responseObject;
	}
}