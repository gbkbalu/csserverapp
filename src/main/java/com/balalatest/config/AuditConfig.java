package com.balalatest.config;

import java.util.Optional;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
public class AuditConfig implements AuditorAware<String> {
	
	public Optional<String> getCurrentAuditor() {
	  
	  final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

	    if (auth == null) {
	      return Optional.of("ANONYMOUS USER");
	    }

	    String name = auth.getName();
	    return Optional.of(name);
		/*final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		if (userPrincipal == null || userPrincipal.getClientUser() == null || userPrincipal.getClientUser().getId() == null) {
			return null;
		}

		String id = userPrincipal.getClientUser().getId();// get logged in user id
		return Optional.of(id);*/
	}

}
