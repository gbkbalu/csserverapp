package com.balalatest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoAction;
import org.springframework.data.mongodb.core.WriteConcernResolver;
import com.mongodb.WriteConcern;

/**
 * Configure custom write concerns for the domain objects based on weather the
 * default write concern is sufficient or not.
 * 
 * @author Shris Infotech.
 *
 */
@Configuration
public class CustomWriteConcernResolver implements WriteConcernResolver {

	@Override
	public WriteConcern resolve(MongoAction action) {
		
		// Configuration goes here.
		// ------------------------
//		if (action.getEntityType().getSimpleName().contains("Audit")) {
//			return WriteConcern.UNACKNOWLEDGED;
//		} else if (action.getEntityType().getSimpleName().contains("Metadata")) {
//			return WriteConcern.ACKNOWLEDGED;
//		}
		return action.getDefaultWriteConcern();
	}

}
