package com.balalatest.constants;

public interface AppConstants
{
	
	public static final int PAGE_LIMIT = 10;
	public static final String FOLDER_NAME = "morganTesting";
}
