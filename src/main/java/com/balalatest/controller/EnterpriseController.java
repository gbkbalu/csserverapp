package com.balalatest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.balalatest.common.QueryUtil;
import com.balalatest.domain.Enterprise;
import com.balalatest.dto.ResultListDto;
import com.balalatest.service.EnterpriseBusinessService;
import com.balalatest.validator.EnterpriseValidator;
import com.balalatest.common.ResponseObject;
	
	/**
	 * REST controller for {@link Enterprise} domain object.
	 * 
	 * 
	 * 
	 * @see Enterprise
	 * @see EnterpriseBusinessLogic#perform(Enterprise, com.balalatest.businessobject.Context)
	 * @see EnterpriseValidator#validate(Object, org.springframework.validation.Errors)
	 * @see EnterpriseBusinessLogic#perform(java.util.Collection, com.balalatest.businessobject.Context)
	 * @see ResultListDto
	 *
	 * @author Shris Infotech
	 */
	 
	@RestController
	@RequestMapping("/enterprise")
	public class EnterpriseController {

		@Autowired
		private EnterpriseBusinessService service;
		
		@Autowired
		private QueryUtil queryUtil;
		
		@Autowired
		private EnterpriseValidator enterpriseValidator;
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@InitBinder
		protected void initBinder(WebDataBinder binder) {
	    	binder.setValidator(enterpriseValidator);
		}
	
		/**
		 * This method is paginated version of {@link EnterpriseController#fetchAllRecords()}
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_VIEW')")
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/records/{page}/{pageSize}")
		public ResultListDto<Enterprise> getAllRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"updatedOn");
	    	ResultListDto<Enterprise> resultListDto = new ResultListDto<Enterprise>();
	    	
			// To Do set, right page limit
			Page<Enterprise> pageList = service.readAll(pageRequest);
			resultListDto.setResultList(pageList.getContent());
			resultListDto.setPageCount(pageList.getTotalPages());
			resultListDto.setTotalCount(pageList.getTotalElements());
			resultListDto.setCurrentPage(page);
			return resultListDto;
			
		}

		/**
		 * Fetches all the entities of type {@link Enterprise} from the data base.
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/fetchAllRecords")
		public ResultListDto<Enterprise> fetchAllRecords() {
		
			final List<Enterprise> pageList = service.fetchAllRecords();
			final ResultListDto<Enterprise> resultListDto = new ResultListDto<Enterprise>();
			resultListDto.setResultList(pageList);
			resultListDto.setTotalCount(pageList.size());
			return resultListDto;
		}
		
		/**
		 * Finds list of {@link Enterprise} objects matching given request JSON
		 * 
		 * @param filterJson
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping("/filterByKeys")
		public ResultListDto<Enterprise> filterByKeys(@RequestBody JSONObject filterJson) {

			final Query queryFromJson = queryUtil.createQueryFromJson(filterJson);
			final ResultListDto<Enterprise> resultListDto = new ResultListDto<Enterprise>();
			resultListDto.setResultList(service.filter(queryFromJson));
			resultListDto.setTotalCount(service.getCount(queryFromJson));
			
			if(filterJson.get("page")!=null) {
				resultListDto.setCurrentPage(Integer.parseInt(filterJson.get("page").toString()));
			}
			
			return resultListDto;
		}

		
		/**
		 * Returns details of {@link Enterprise}} for a given id
		 * 
		 * @param id
		 * @return {@link Enterprise}} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('EMP_VIEW')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping
		public Enterprise read(@RequestParam String id) {
			logger.info("Reading a Enterprise with id: " + id);
			return service.read(id);
		}
		
		/**
		 * Creates new {@link Emp} in the database.
		 * 
		 * @param entity
		 * @return Newly created {@link Enterprise} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_CREATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_CREATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping
		public Enterprise create(@Valid @RequestBody Enterprise entity) {
			logger.info("Creating a new Enterprise: ");
			return service.create(entity);
		}
	
		/**
		 * Updates an existing {@link Enterprise} in the database.
		 * 
		 * @param entity {@link Enterprise} to update
		 * @return Updated {@link Enterprise} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_UPDATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_UPDATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PutMapping
		public Enterprise update(@Valid @RequestBody Enterprise entity) {
			logger.info("Update Enterprise: ");
			return service.update(entity);
		}

        /**
		 * Deletes an existing {@link MarketData} from the database. Delete Multiple at once.
		 * @param objectIds
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('COUNTRY_DELETE')")
		 */
		
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping("/deleteAll")
		public List<ResponseObject> deleteAll(@RequestBody String objectIds[]) {
			
			List<ResponseObject> responseObjectsList = new LinkedList<ResponseObject>();
			for(String id:objectIds)
			{
				ResponseObject responseObject = queryUtil.getResponseObject(id);
				responseObject.setStatus(delete(id));
				
				responseObjectsList.add(responseObject);
			}
			
			return responseObjectsList;
		}
		
		/**
		 * Deletes an existing {@link MarketData} from the database.
		 * 
		 * @param id {@link MarketData} object with the matching id passed in will be deleted..
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ENTERPRISE_DELETE')")
		 */
		//@PreAuthorize("hasAuthority('ENTERPRISE_DELETE')")
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping
		public Boolean delete(@RequestParam String id) {
			logger.info("Delete Enterprise: ");
			return service.delete(id);
		}
	}


