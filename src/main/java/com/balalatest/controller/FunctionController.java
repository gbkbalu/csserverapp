package com.balalatest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.balalatest.common.QueryUtil;
import com.balalatest.domain.Function;
import com.balalatest.dto.ResultListDto;
import com.balalatest.service.FunctionBusinessService;
import com.balalatest.validator.FunctionValidator;
import com.balalatest.common.ResponseObject;
	
	/**
	 * REST controller for {@link Function} domain object.
	 * 
	 * 
	 * 
	 * @see Function
	 * @see FunctionBusinessLogic#perform(Function, com.balalatest.businessobject.Context)
	 * @see FunctionValidator#validate(Object, org.springframework.validation.Errors)
	 * @see FunctionBusinessLogic#perform(java.util.Collection, com.balalatest.businessobject.Context)
	 * @see ResultListDto
	 *
	 * @author Shris Infotech
	 */
	 
	@RestController
	@RequestMapping("/function")
	public class FunctionController {

		@Autowired
		private FunctionBusinessService service;
		
		@Autowired
		private QueryUtil queryUtil;
		
		@Autowired
		private FunctionValidator functionValidator;
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@InitBinder
		protected void initBinder(WebDataBinder binder) {
	    	binder.setValidator(functionValidator);
		}
	
		/**
		 * This method is paginated version of {@link FunctionController#fetchAllRecords()}
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_VIEW')")
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/records/{page}/{pageSize}")
		public ResultListDto<Function> getAllRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"updatedOn");
	    	ResultListDto<Function> resultListDto = new ResultListDto<Function>();
	    	
			// To Do set, right page limit
			Page<Function> pageList = service.readAll(pageRequest);
			resultListDto.setResultList(pageList.getContent());
			resultListDto.setPageCount(pageList.getTotalPages());
			resultListDto.setTotalCount(pageList.getTotalElements());
			resultListDto.setCurrentPage(page);
			return resultListDto;
			
		}

		/**
		 * Fetches all the entities of type {@link Function} from the data base.
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/fetchAllRecords")
		public ResultListDto<Function> fetchAllRecords() {
		
			final List<Function> pageList = service.fetchAllRecords();
			final ResultListDto<Function> resultListDto = new ResultListDto<Function>();
			resultListDto.setResultList(pageList);
			resultListDto.setTotalCount(pageList.size());
			return resultListDto;
		}
		
		/**
		 * Finds list of {@link Function} objects matching given request JSON
		 * 
		 * @param filterJson
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping("/filterByKeys")
		public ResultListDto<Function> filterByKeys(@RequestBody JSONObject filterJson) {

			final Query queryFromJson = queryUtil.createQueryFromJson(filterJson);
			final ResultListDto<Function> resultListDto = new ResultListDto<Function>();
			resultListDto.setResultList(service.filter(queryFromJson));
			resultListDto.setTotalCount(service.getCount(queryFromJson));
			
			if(filterJson.get("page")!=null) {
				resultListDto.setCurrentPage(Integer.parseInt(filterJson.get("page").toString()));
			}
			
			return resultListDto;
		}

		
		/**
		 * Returns details of {@link Function}} for a given id
		 * 
		 * @param id
		 * @return {@link Function}} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('EMP_VIEW')")
		 *
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping
		public Function read(@RequestParam String id) {
			logger.info("Reading a Function with id: " + id);
			return service.read(id);
		}
		
		/**
		 * Creates new {@link Emp} in the database.
		 * 
		 * @param entity
		 * @return Newly created {@link Function} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_CREATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_CREATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping
		public Function create(@Valid @RequestBody Function entity) {
			logger.info("Creating a new Function: ");
			return service.create(entity);
		}
	
		/**
		 * Updates an existing {@link Function} in the database.
		 * 
		 * @param entity {@link Function} to update
		 * @return Updated {@link Function} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_UPDATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_UPDATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PutMapping
		public Function update(@Valid @RequestBody Function entity) {
			logger.info("Update Function: ");
			return service.update(entity);
		}

        /**
		 * Deletes an existing {@link MarketData} from the database. Delete Multiple at once.
		 * @param objectIds
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('COUNTRY_DELETE')")
		 */
		
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping("/deleteAll")
		public List<ResponseObject> deleteAll(@RequestBody String objectIds[]) {
			
			List<ResponseObject> responseObjectsList = new LinkedList<ResponseObject>();
			for(String id:objectIds)
			{
				ResponseObject responseObject = queryUtil.getResponseObject(id);
				responseObject.setStatus(delete(id));
				
				responseObjectsList.add(responseObject);
			}
			
			return responseObjectsList;
		}
		
		/**
		 * Deletes an existing {@link MarketData} from the database.
		 * 
		 * @param id {@link MarketData} object with the matching id passed in will be deleted..
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('FUNCTION_DELETE')")
		 */
		//@PreAuthorize("hasAuthority('FUNCTION_DELETE')")
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping
		public Boolean delete(@RequestParam String id) {
			logger.info("Delete Function: ");
			return service.delete(id);
		}
	}


