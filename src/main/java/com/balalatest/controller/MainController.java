package com.balalatest.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author 
 */
@Controller
public class MainController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody String defaultPage(ModelMap map) {
		return "{\"message\":\"Login First\"}";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "/logoutSuccess", method = RequestMethod.GET)
	public @ResponseBody String logoutSuccess() {
		SecurityContextHolder.clearContext();
		return "logout";
	}

	@RequestMapping(value = "/loginFirst")
	public @ResponseBody String loginFirst() {
		return "{\"message\":\"login First\"}";
	}
	
	@RequestMapping(value = "/accessdenied")
	public @ResponseBody String loginerror() {
		return "{\"message\":\"Access Denied\"}";
	}
	
	@RequestMapping(value = "/longinSuccess")
	public @ResponseBody String longinSuccess() {
		return "{\"message\":\"login success\"}";
	}
	
	@RequestMapping(value = "/loginFailure")
	public @ResponseBody String loginFailure(HttpServletRequest request,HttpServletResponse response) {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		return "{\"message\":\"Invlid Credentials\"}";
	}
	
	@RequestMapping(value = "/isLoggedIn")
	public @ResponseBody String isLoggedIn() {
		return "{\"message\":\"yes\"}";
	}
	
	@RequestMapping(value = "/getAuthoritiesList")
	public @ResponseBody LinkedList<String> getAuthoritiesList() {
		Collection<? extends GrantedAuthority> authorityList = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		
		LinkedList<String> authList = new LinkedList<String>();
		if(authorityList != null && authorityList.size()>0)
		{
			Iterator itr = authorityList.iterator();
			
			while(itr.hasNext()) 
			{
				GrantedAuthority authority = (GrantedAuthority) itr.next();
				System.out.print(authority.getAuthority());
				authList.add(authority.getAuthority());
			}
		}
		return authList;
	}
}


