package com.balalatest.controller;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.balalatest.businessobject.OrderBusinessLogic;
import com.balalatest.common.QueryUtil;
import com.balalatest.common.ResponseObject;
import com.balalatest.domain.AggregateOrder;
import com.balalatest.domain.MarketData;
import com.balalatest.domain.Order;
import com.balalatest.domain.OrderData;
import com.balalatest.domain.SparkOrderData;
import com.balalatest.domain.TotalCount;
import com.balalatest.dto.MarketOrderData;
import com.balalatest.dto.ResultListDto;
import com.balalatest.service.OrderBusinessService;
import com.balalatest.validator.OrderValidator;
	
	/**
	 * REST controller for {@link Order} domain object.
	 * 
	 * 
	 * 
	 * @see Order
	 * @see OrderBusinessLogic#perform(Order, com.shris.businessobject.Context)
	 * @see OrderValidator#validate(Object, org.springframework.validation.Errors)
	 * @see OrderBusinessLogic#perform(java.util.Collection, com.shris.businessobject.Context)
	 * @see ResultListDto
	 *
	 * @author Shris Infotech
	 */
	 
	@Controller
	@RequestMapping("/order")
	public class OrderController {

		@Autowired
		private OrderBusinessService service;
		
		@Autowired
		private QueryUtil queryUtil;
		
		@Autowired
		private OrderValidator orderValidator;
		
		@Autowired
		private MongoTemplate mongoTemplate;
		
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@InitBinder
		protected void initBinder(WebDataBinder binder) {
	    	binder.setValidator(orderValidator);
		}
	
		/**
		 * This method is paginated version of {@link OrderController#fetchAllRecords()}
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ORDER_VIEW')")
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(value = "/records/{page}/{pageSize}")
		public @ResponseBody ResultListDto<AggregateOrder> getAllRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"orderDate");
	    	ResultListDto<AggregateOrder> resultListDto = new ResultListDto<AggregateOrder>();
			
			resultListDto.setResultList(fetchUsingAggregation(pageRequest));
			
			//int totalCount = fetchTotalCount(pageRequest);
			//resultListDto.setPageCount(totalCount/pageSize);
			//resultListDto.setTotalCount(totalCount);
			resultListDto.setCurrentPage(page);
			return resultListDto;
			
		}
		
		public List<AggregateOrder> fetchUsingAggregation(PageRequest pageRequest)
		{
			Fields fields = Fields.fields("ticker", "accountId", "clientId")
					.and("orderDataList.qty", "orderDataList.qty")
					.and("orderDataList.side", "orderDataList.side")
					.and("orderDataList.orderDate", "orderDataList.orderDate")
					.and("orderDataList.msg", "orderDataList.msg")
					.and("orderDataList.tagValues", "orderDataList.tagValues")
					.and("orderDataList.orderId", "orderDataList.orderId")
					.and("orderDataList.totalTimeTaken", "orderDataList.totalTimeTaken")
					
					;
			
			Aggregation aggrObj = newAggregation(
					Aggregation.unwind("$orderDataList"),
					Aggregation.skip(pageRequest.getPageNumber()*pageRequest.getPageSize()),
					Aggregation.limit(pageRequest.getPageSize()),
					project(fields)
			);
			
			
			AggregationResults<AggregateOrder> aggrigationResult = mongoTemplate.aggregate(aggrObj, Order.class, AggregateOrder.class);
			List<AggregateOrder> ordersList = aggrigationResult.getMappedResults();
			
			return ordersList;
		}
		
		public int fetchTotalCount(PageRequest pageRequest)
		{
			Aggregation aggrObj = newAggregation(
					Aggregation.unwind("$orderDataList"),
					Aggregation.group().count().as("total")
			);
			
			AggregationResults<TotalCount> aggrigationResult = mongoTemplate.aggregate(aggrObj, Order.class, TotalCount.class);
			List<TotalCount> countList = aggrigationResult.getMappedResults();
			
			if(countList != null && !countList.isEmpty())
			{
				return countList.get(0).getTotal();
			}
			
			return 0;
		}
		
		@RequestMapping(value = "/getFromSparkSqlRecords/{page}/{pageSize}")
		public @ResponseBody ResultListDto<SparkOrderData> getFromSparkSqlRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			//pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"orderDate");
	    	ResultListDto<SparkOrderData> resultListDto = new ResultListDto<SparkOrderData>();
			
			//resultListDto.setResultList(fetchUsingSparkSql(pageRequest));
			
			return resultListDto;
		}
		
		@RequestMapping(value = "/getFromSparkRecords/{page}/{pageSize}")
		public @ResponseBody ResultListDto<SparkOrderData> getFromSparkRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			//pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"orderDate");
	    	ResultListDto<SparkOrderData> resultListDto = new ResultListDto<SparkOrderData>();
			//resultListDto.setResultList(fetchUsingSparkSql1(pageRequest,"N", null, null, null));
			return resultListDto;
		}
		
		/**
		 * Fetches all the entities of type {@link Order} from the data base.
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ORDER_VIEW')") 
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(value = "/fetchAllRecords")
		public @ResponseBody ResultListDto<Order> fetchAllRecords() {
		
			final List<Order> pageList = service.fetchAllRecords();
			final ResultListDto<Order> resultListDto = new ResultListDto<Order>();
			resultListDto.setResultList(pageList);
			return resultListDto;
		}
		
		@RequestMapping(value = "/filterByOrderId", method = RequestMethod.POST)
		public @ResponseBody MarketOrderData filterByOrderId(@RequestBody JSONObject filterJson) {
			ResultListDto<AggregateOrder> resultListDto = filterByKeys(filterJson);
			
			MarketOrderData marketOrderData = new MarketOrderData();
			marketOrderData = getMarketOrderDataList(resultListDto,marketOrderData);
			return marketOrderData;
		}
		
		@RequestMapping(value = "/filterByOrderId1", method = RequestMethod.POST)
		public MarketOrderData filterByOrderId1(@RequestBody JSONObject filterJson) {
			
			ResultListDto<AggregateOrder> resultListDto = filterByKeys(filterJson);
			
			MarketOrderData marketOrderData = new MarketOrderData();
			
			String orderId = filterJson.get("orderDataList.orderId").toString();
			/*SparkSession spark = SparkKafkaStreamConsumer.createSparkSessionForSpring();
			
			Dataset<LogStashRecord> logsData = spark.read().format("org.elasticsearch.spark.sql")
					.option("es.read.field.include","ticker,clientId,orderId,accountId,exchange,startDateTime,endDateTime")
					.load("logcsserver").where((new Column("orderId")).$eq$eq$eq(orderId)).as(Encoders.bean(LogStashRecord.class));*/
			//List<LogStashRecord> logstashList = logsData.collectAsList();
			
			
			//marketOrderData.setLogStashRecordsList(logstashList);
			marketOrderData = getMarketOrderDataList(resultListDto,marketOrderData);
			
			//spark.close();
			return marketOrderData;
		}
		
		public MarketOrderData getMarketOrderDataList(ResultListDto<AggregateOrder> resultListDto,MarketOrderData marketOrderData)
		{
			if(resultListDto == null || resultListDto.getResultList().isEmpty())
			{
				return marketOrderData;
			}
			
			AggregateOrder aggregateOrder = resultListDto.getResultList().get(0);
			OrderData orderData = aggregateOrder.getOrderDataList();
			Query query = new Query(Criteria.where("ticker").is(aggregateOrder.getTicker()));
			Date startDate = orderData.getOrderDate();
			Date endDate = new Date(startDate.getTime());
			endDate.setHours(23);
			endDate.setMinutes(59);
			endDate.setSeconds(59);
			
			query.addCriteria(Criteria.where("marketDate").gte(startDate).lte(endDate));
			//query.addCriteria(Criteria.where("price").lt(orderData.getPrice()));
			query.with(new Sort(Sort.Direction.ASC, "price"));
			///query.limit(1);
			List<MarketData> marketDataList = mongoTemplate.find(query, MarketData.class);
			marketOrderData.setMarketDataList(marketDataList);
			marketOrderData.setAggregateOrder(aggregateOrder);
			
			return marketOrderData;
		}
		
		/**
		 * Finds list of {@link Order} objects matching given request JSON
		 * 
		 * @param filterJson
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ORDER_VIEW')") 
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(value = "/filterByKeys", method = RequestMethod.POST)
		public @ResponseBody ResultListDto<AggregateOrder> filterByKeys(@RequestBody JSONObject filterJson) {

			int page = 0;
			int pageSize = 1;
			
			if(filterJson.get("page")!=null) {
				page = Integer.parseInt(filterJson.get("page").toString());
			}
			
			if(filterJson.get("pageSize")!=null) {
				pageSize = Integer.parseInt(filterJson.get("pageSize").toString());
			}
			
			JSONObject filterJson1 = new JSONObject();
			JSONObject filterJson2 = new JSONObject();
			
			Set keys = filterJson.keySet();
			Object[] keysArray = keys.toArray();
			String key = null;
			Object value = null;
			int iteLen = 0;
			for(int index=0;index<keys.size();index++) 
			{
				key = keysArray[index].toString();
				value = filterJson.get(key);
				
				if(key.equalsIgnoreCase("page") || key.equalsIgnoreCase("pageSize"))
				{
					filterJson1.put(key, value);
					filterJson2.put(key, value);
				}else
				{
					if(!key.contains("."))
					{
						filterJson1.put(key, value);
					}else
					{
						filterJson2.put(key, value);
					}
				}
				
			}
			
			final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"orderDate");

			final Query queryFromJson = queryUtil.createQueryFromJson(filterJson);
			final Criteria criteriaFromJson = new Criteria();//queryUtil.createCriteriaFromJson(filterJson1);
			final Criteria criteriaFromJson1 = new Criteria();//queryUtil.createCriteriaFromJson(filterJson2);
			final ResultListDto<AggregateOrder> resultListDto = new ResultListDto<AggregateOrder>();
			
			
			if(filterJson.get("page")!=null) {
				resultListDto.setCurrentPage(Integer.parseInt(filterJson.get("page").toString()));
			}
			
			
			List<AggregateOrder> aggOrdList = filterUsingAggregation(criteriaFromJson,criteriaFromJson1, pageRequest,filterJson);
			List<AggregateOrder> copyAggOrdList = new ArrayList<AggregateOrder>();
			if(filterJson.containsKey("orderDataList.orderId") && !aggOrdList.isEmpty())
			{
				String orderId = filterJson.get("orderDataList.orderId").toString();
				for(AggregateOrder aggregateOrder:aggOrdList)
				{
					if(aggregateOrder.getOrderDataList() != null && aggregateOrder.getOrderDataList().getOrderId() != null && orderId.equalsIgnoreCase(aggregateOrder.getOrderDataList().getOrderId()))
					{
						copyAggOrdList.add(aggregateOrder);
						break;
					}
				}
			}else
			{
				copyAggOrdList = aggOrdList;
			}
			
			
			resultListDto.setResultList(copyAggOrdList);
			
			//int totalCount = filterFetchTotalCount(criteriaFromJson);
			//resultListDto.setTotalCount(totalCount);
			//resultListDto.setPageCount(totalCount/pageSize);
			resultListDto.setCurrentPage(page);
						
			return resultListDto;
		}
		
		public List<AggregateOrder> filterUsingAggregation(Criteria criteria,Criteria criteria1, PageRequest pageRequest,JSONObject filterJson)
		{
			Fields fields = Fields.fields("ticker", "accountId", "clientId")
					.and("orderDataList.qty", "orderDataList.qty")
					.and("orderDataList.side", "orderDataList.side")
					.and("orderDataList.orderDate", "orderDataList.orderDate")
					.and("orderDataList.msg", "orderDataList.msg")
					.and("orderDataList.tagValues", "orderDataList.tagValues")
					.and("orderDataList.orderId", "orderDataList.orderId")
					.and("orderDataList.totalTimeTaken", "orderDataList.totalTimeTaken")
					;
			
			Aggregation aggrObj = newAggregation(match(criteria),
					Aggregation.unwind("$orderDataList"),
					match(criteria1),
					Aggregation.skip(pageRequest.getPageNumber()*pageRequest.getPageSize()),
					Aggregation.limit(pageRequest.getPageSize()),
					project(fields)
			);
			
			AggregationResults<AggregateOrder> aggrigationResult = mongoTemplate.aggregate(aggrObj, Order.class, AggregateOrder.class);
			List<AggregateOrder> ordersList = aggrigationResult.getMappedResults();
			
			return ordersList;
		}
		
		public int filterFetchTotalCount(Criteria criteria)
		{
			Aggregation aggrObj = newAggregation(
					match(criteria),
					Aggregation.unwind("$orderDataList"),
					Aggregation.group().count().as("total")
			);
			
			AggregationResults<TotalCount> aggrigationResult = mongoTemplate.aggregate(aggrObj, Order.class, TotalCount.class);
			List<TotalCount> countList = aggrigationResult.getMappedResults();
			
			if(countList != null && !countList.isEmpty())
			{
				return countList.get(0).getTotal();
			}
			
			return 0;
		}

		
		/**
		 * Returns details of {@link Order}} for a given id
		 * 
		 * @param id
		 * @return {@link Order}} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('EMP_VIEW')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ORDER_VIEW')") 
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(method = RequestMethod.GET)
		public @ResponseBody Order read(@RequestParam String id) {
			logger.info("Reading a Order with id: " + id);
			Order order = new Order();
			order.setId(id);
			return service.read(order);
		}
		
		/**
		 * Creates new {@link Emp} in the database.
		 * 
		 * @param entity
		 * @return Newly created {@link Order} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_CREATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ORDER_CREATE')") 
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(method = RequestMethod.POST)
		public @ResponseBody Order create(@Valid @RequestBody Order entity) {
			logger.info("Creating a new Order: ");
			return service.create(entity);
		}
	
		/**
		 * Updates an existing {@link Order} in the database.
		 * 
		 * @param entity {@link Order} to update
		 * @return Updated {@link Order} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_UPDATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ORDER_UPDATE')") 
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(method = RequestMethod.PUT)
		public @ResponseBody Order update(@Valid @RequestBody Order entity) {
			logger.info("Update Order: ");
			return service.update(entity);
		}

        /**
		 * Deletes an existing {@link MarketData} from the database. Delete Multiple at once.
		 * @param objectIds
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('COUNTRY_DELETE')")
		 */
		
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(value = "/deleteAll",method = RequestMethod.DELETE)
		public @ResponseBody List<ResponseObject> deleteAll(@RequestBody String objectIds[]) {
			
			List<ResponseObject> responseObjectsList = new LinkedList<ResponseObject>();
			for(String id:objectIds)
			{
				ResponseObject responseObject = queryUtil.getResponseObject(id);
				responseObject.setStatus(delete(id));
				
				responseObjectsList.add(responseObject);
			}
			
			return responseObjectsList;
		}
		
		/**
		 * Deletes an existing {@link MarketData} from the database.
		 * 
		 * @param id {@link MarketData} object with the matching id passed in will be deleted..
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ORDER_DELETE')")
		 */
		//@PreAuthorize("hasAuthority('ORDER_DELETE')")
		//@PreAuthorize("hasAuthority('Admin')")
		@RequestMapping(method = RequestMethod.DELETE)
		public @ResponseBody Boolean delete(@RequestParam String id) {
			Order order = new Order();
			order.setId(id);
			logger.info("Delete Order: ");
			return service.delete(order);
		}
	}


