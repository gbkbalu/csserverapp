package com.balalatest.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import com.balalatest.domain.Order;
import com.balalatest.domain.OrderData;
import com.balalatest.domain.SparkOrderData;

public class OrderUtil {

  private static int callCount = 1;
  private static String currentTicker, currentClient, currentAccount;
  private static int nextClientIndex = 0, nextAccountIndex = 0;
  private static Random rnd = new Random();
  private static String[] tickerArray =
      new String[] {"AAPL", "AMZN", "CMCSA", "CVX", "LUV", "JPM", "NVDA", "GOOG", "MSFT", "XOM"};
  private static String[] clientIdArr =
      new String[] {"ABYL", "BAUM", "CAZE", "DBAT", "ENCL", "GCCG", "IOIX", "KPAR", "XFAS", "ZOUL"};
  private static String[] accountIdArray = new String[] {"ACCT0001", "ACCT0002", "ACCT0003",
      "ACCT0004", "ACCT0005", "ACCT0006", "ACCT0007", "ACCT0008", "ACCT0009", "ACCT0010",
      "ACCT0011", "ACCT0012", "ACCT0013", "ACCT0014", "ACCT0015", "ACCT0016", "ACCT0017",
      "ACCT0018", "ACCT0019", "ACCT0020", "ACCT0021", "ACCT0022", "ACCT0023", "ACCT0024",
      "ACCT0025", "ACCT0026", "ACCT0027", "ACCT0028", "ACCT0029", "ACCT0030", "ACCT0031",
      "ACCT0032", "ACCT0033", "ACCT0034", "ACCT0035", "ACCT0036", "ACCT0037", "ACCT0038",
      "ACCT0039", "ACCT0040", "ACCT0041", "ACCT0042", "ACCT0043", "ACCT0044", "ACCT0045",
      "ACCT0046", "ACCT0047", "ACCT0048", "ACCT0049", "ACCT0050", "ACCT0051", "ACCT0052",
      "ACCT0053", "ACCT0054", "ACCT0055", "ACCT0056", "ACCT0057", "ACCT0058", "ACCT0059",
      "ACCT0060", "ACCT0061", "ACCT0062", "ACCT0063", "ACCT0064", "ACCT0065", "ACCT0066",
      "ACCT0067", "ACCT0068", "ACCT0069", "ACCT0070", "ACCT0071", "ACCT0072", "ACCT0073",
      "ACCT0074", "ACCT0075", "ACCT0076", "ACCT0077", "ACCT0078", "ACCT0079", "ACCT0080",
      "ACCT0081", "ACCT0082", "ACCT0083", "ACCT0084", "ACCT0085", "ACCT0086", "ACCT0087",
      "ACCT0088", "ACCT0089", "ACCT0090", "ACCT0091", "ACCT0092", "ACCT0093", "ACCT0094",
      "ACCT0095", "ACCT0096", "ACCT0097", "ACCT0098", "ACCT0099", "ACCT0100"};



  public static int getCallCount() {
    return callCount;
  }

  public static void setCallCount(int callCount) {
    OrderUtil.callCount = callCount;
  }

  public static String getCurrentTicker() {
    return currentTicker;
  }

  public static void setCurrentTicker() {
    OrderUtil.currentTicker = tickerArray[rnd.nextInt(10)];
  }

  public static String getCurrentClient() {
    return currentClient;
  }

  public static void setCurrentClient(String currentClient) {
    OrderUtil.currentClient = currentClient;
  }

  public static String getCurrentAccount() {
    return currentAccount;
  }

  public static void setCurrentAccount(String currentAccount) {
    OrderUtil.currentAccount = currentAccount;
  }

  public static int getNextClientIndex() {
    return nextClientIndex;
  }

  public static void setNextClientIndex(int nextClientIndex) {
    OrderUtil.nextClientIndex = nextClientIndex;
  }

  public static int getNextAccountIndex() {
    return nextAccountIndex;
  }

  public static void setNextAccountIndex(int nextAccountIndex) {
    OrderUtil.nextAccountIndex = nextAccountIndex;
  }

  public static Random getRnd() {
    return rnd;
  }

  public static void setRnd(Random rnd) {
    OrderUtil.rnd = rnd;
  }

  public static String[] getTickerArray() {
    return tickerArray;
  }

  public static void setTickerArray(String[] tickerArray) {
    OrderUtil.tickerArray = tickerArray;
  }

  public static String[] getClientIdArr() {
    return clientIdArr;
  }

  public static void setClientIdArr(String[] clientIdArr) {
    OrderUtil.clientIdArr = clientIdArr;
  }

  public static String[] getAccountIdArray() {
    return accountIdArray;
  }

  public static void setAccountIdArray(String[] accountIdArray) {
    OrderUtil.accountIdArray = accountIdArray;
  }

  public static List<Order> getOrderData() {

    List<Order> ordersList = new LinkedList<>();
    for (String tickerName : tickerArray) {

      for (String clientId : clientIdArr) {

        for (String accountId : accountIdArray) {

          Order order = new Order();
          order.setTicker(tickerName);
          order.setClientId(clientId);
          order.setAccountId(accountId);

          List<OrderData> orderDataList = new LinkedList<OrderData>();
          
          OrderData orderData = new OrderData();
          orderData.setOrderId(UUID.randomUUID().toString());
          orderData.setQty((int)(Math.random()*(99-10+1)+10));
          orderData.setPrice((Math.random()*(200-100+1)+100));
          orderData.setOrderDate(new Date());
          
          orderDataList.add(orderData);
          order.setOrderDataList(orderDataList);
          ordersList.add(order);
        }

      }

    }
    return ordersList;
  }

  public static OrderData convert(String msg, long recordCount, long bucketCount, long bucketSize,
      int numberOfDocumentsInsered, Date orderDate, Map<String, List<OrderData>> bucketMap) {

    if (currentTicker == null) {
      currentTicker = tickerArray[rnd.nextInt(10)];
    }

    if (recordCount % bucketSize == 0) {
      // Get next random ticker.
      // currentTicker = tickerArray[rnd.nextInt(10)];
    }
    /*
     * StringBuilder sb = new StringBuilder(); // Client Id + "#" + Account Id + "#" + Ticker + "#"
     * + Trade Date sb.append(currentClient).append("#").append(currentAccount).append("#")
     * .append(currentTicker).append("#").append(getDateString(orderDate));
     * 
     * String key = sb.toString();
     * 
     * if (bucketMap != null && bucketMap.containsKey(key)) { List<OrderData> orderDataList =
     * bucketMap.get(key); if(orderDataList != null && orderDataList.size() == 100) { // Get next
     * random ticker. currentTicker = tickerArray[rnd.nextInt(10)];
     * System.out.println("Next>>>>....recordCount = " + recordCount + " bucket size=" + bucketSize
     * + " currentTicker =" + currentTicker); } }
     */

    // nextAccountIndex = numberOfDocumentsInsered % 100;
    nextClientIndex = nextAccountIndex / 10;

    currentAccount = accountIdArray[nextAccountIndex];
    currentClient = clientIdArr[nextClientIndex];

    return convert(msg, currentClient, currentAccount, currentTicker, orderDate);
  }

  public static OrderData convert(String msg, String clientId, String accountId, String ticker,
      Date orderDate) {
    OrderData orderData = new OrderData();
    orderData.setMsg(msg);

    String[] tokens = msg.split("\\x01");
    for (int i = 0; i < tokens.length; i++) {
      final String token = tokens[i];
      String[] keyValeArr = token.split("=");
      final String tag = keyValeArr[0];

      switch (tag) {
        // Order Date
        case "999":
          // To Do parse it as date
          if (orderDate == null) {
            orderData.setOrderDate(parseInputDate(keyValeArr[1]));
          } else {
            orderData.setOrderDate(orderDate);
          }

          break;
        // Order Id
        case "11":
          orderData.setOrderId(keyValeArr[1]);
          break;
        // Quantity
        case "38":
          orderData.setQty(Integer.parseInt(keyValeArr[1]));
          break;
        // Client Id
        case "49":
          if (clientId == null) {
            orderData.setClientId(keyValeArr[1]);
          } else {
            orderData.setClientId(clientId);
          }
          break;
        // Account Id
        case "1":
          if (accountId == null) {
            orderData.setAccountId(keyValeArr[1]);
          } else {
            orderData.setAccountId(accountId);
          }
          break;
        // Ticker
        case "55":
          if (ticker == null) {
            orderData.setTicker(keyValeArr[1]);
          } else {
            orderData.setTicker(ticker);
          }
          break;
        // Side
        case "54":
          orderData.setSide(keyValeArr[1]);
          break;
        // Original Client OrderID (On cxl/replace or cancel orders)
        case "41":
          // orderData.setSide(keyValeArr[1]);
          break;
        case "44":
          orderData.setPrice(Double.parseDouble(keyValeArr[1]));
          break;
        default:
          orderData.getTagValues().put("tag" + tag, keyValeArr[1]);
          break;
      }
    }

    orderData.setDateString(getDateString(orderData.getOrderDate()));
    return orderData;

  }

  public static SparkOrderData convertToSparkOrder(String msg, String clientId, String accountId,
      String ticker, Date orderDate) {
    SparkOrderData orderData = new SparkOrderData();
    orderData.setMsg(msg);

    String[] tokens = msg.split("\\x01");
    for (int i = 0; i < tokens.length; i++) {
      final String token = tokens[i];
      String[] keyValeArr = token.split("=");
      final String tag = keyValeArr[0];

      switch (tag) {
        // Order Date
        case "52":
          // To Do parse it as date
          if (orderDate == null) {
            // orderData.setOrderDate(parseOrderDate(keyValeArr[1]).getTime());
            orderData.setOrderDate(parseInputDate(keyValeArr[1]).getTime());
          } else {
            orderData.setOrderDate(orderDate.getTime());
          }

          break;
        // Order Id
        case "11":
          orderData.setOrderId(keyValeArr[1] + "" + rnd.nextInt(999));
          break;
        // Quantity
        case "38":
          orderData.setQty(Integer.parseInt(keyValeArr[1]));
          break;
        // Client Id
        case "49":
          if (clientId == null) {
            orderData.setClientId(keyValeArr[1]);
          } else {
            orderData.setClientId(clientId);
          }
          break;
        // Account Id
        case "1":
          if (accountId == null) {
            orderData.setAccountId(keyValeArr[1]);
          } else {
            orderData.setAccountId(accountId);
          }
          break;
        // Ticker
        case "55":
          if (ticker == null) {
            orderData.setTicker(keyValeArr[1]);
          } else {
            orderData.setTicker(ticker);
          }
          break;
        // Side
        case "54":
          orderData.setSide(keyValeArr[1]);
          break;
        // Original Client OrderID (On cxl/replace or cancel orders)
        case "41":
          orderData.setSide(keyValeArr[1]);
          break;
        default:
          orderData.getTagValues().put("tag" + tag, keyValeArr[1]);
          break;
      }
    }

    orderData.setDateString(getDateString(orderData.getOrderDate()));
    return orderData;

  }


  public static Order convertToOrder(OrderData orderData) {
    Order order = new Order();

    order.setClientId(orderData.getClientId());
    order.setAccountId(orderData.getAccountId());
    order.setTicker(orderData.getTicker());
    // order.setId(orderData.getClientId() + orderData.getAccountId() +
    // getDateString(orderData.getOrderDate())
    // + orderData.getTicker() + orderData.getOrderId());
    order.setOrderDate(OrderUtil.removeTime(orderData.getOrderDate()));
    return order;

  }

  private static Date parseOrderDate(String dateString) {

    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.DAY_OF_MONTH, -1 * callCount);
    callCount++;

    if (callCount == 3) {
      callCount = 1;
    }

    return cal.getTime();
    /*
     * DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); try { Date date
     * = inputFormatter.parse(dateString); //date = removeTime(date); return date; } catch
     * (Exception e) { System.out.println(e.getMessage()); e.printStackTrace(); } return new Date();
     */
  }

  public static Date removeTime(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.getTime();
  }

  public static String getDateString(Date date) {

    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    StringBuilder sb = new StringBuilder(cal.get(Calendar.YEAR) + "");
    sb.append("-");
    sb.append(cal.get(Calendar.MONTH));
    sb.append("-");
    sb.append(cal.get(Calendar.DAY_OF_MONTH));
    return sb.toString();
  }

  public static Date parseInputDate(String dateString) {
    DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
    Date date = null;
    try {
      date = inputFormatter.parse(dateString);
      return date;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
    return new Date();
  }

}
