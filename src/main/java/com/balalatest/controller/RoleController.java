package com.balalatest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.balalatest.common.QueryUtil;
import com.balalatest.domain.Role;
import com.balalatest.dto.ResultListDto;
import com.balalatest.service.RoleBusinessService;
import com.balalatest.validator.RoleValidator;
import com.balalatest.common.ResponseObject;
	
	/**
	 * REST controller for {@link Role} domain object.
	 * 
	 * 
	 * 
	 * @see Role
	 * @see RoleBusinessLogic#perform(Role, com.balalatest.businessobject.Context)
	 * @see RoleValidator#validate(Object, org.springframework.validation.Errors)
	 * @see RoleBusinessLogic#perform(java.util.Collection, com.balalatest.businessobject.Context)
	 * @see ResultListDto
	 *
	 * @author Shris Infotech
	 */
	 
	@RestController
	@RequestMapping("/role")
	public class RoleController {

		@Autowired
		private RoleBusinessService service;
		
		@Autowired
		private QueryUtil queryUtil;
		
		@Autowired
		private RoleValidator roleValidator;
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@InitBinder
		protected void initBinder(WebDataBinder binder) {
	    	binder.setValidator(roleValidator);
		}
	
		/**
		 * This method is paginated version of {@link RoleController#fetchAllRecords()}
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ROLE_VIEW')")
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/records/{page}/{pageSize}")
		public ResultListDto<Role> getAllRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"updatedOn");
	    	ResultListDto<Role> resultListDto = new ResultListDto<Role>();
	    	
			// To Do set, right page limit
			Page<Role> pageList = service.readAll(pageRequest);
			resultListDto.setResultList(pageList.getContent());
			resultListDto.setPageCount(pageList.getTotalPages());
			resultListDto.setTotalCount(pageList.getTotalElements());
			resultListDto.setCurrentPage(page);
			return resultListDto;
			
		}

		/**
		 * Fetches all the entities of type {@link Role} from the data base.
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ROLE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/fetchAllRecords")
		public ResultListDto<Role> fetchAllRecords() {
		
			final List<Role> pageList = service.fetchAllRecords();
			final ResultListDto<Role> resultListDto = new ResultListDto<Role>();
			resultListDto.setResultList(pageList);
			resultListDto.setTotalCount(pageList.size());
			return resultListDto;
		}
		
		/**
		 * Finds list of {@link Role} objects matching given request JSON
		 * 
		 * @param filterJson
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('ROLE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping("/filterByKeys")
		public ResultListDto<Role> filterByKeys(@RequestBody JSONObject filterJson) {

			final Query queryFromJson = queryUtil.createQueryFromJson(filterJson);
			final ResultListDto<Role> resultListDto = new ResultListDto<Role>();
			resultListDto.setResultList(service.filter(queryFromJson));
			resultListDto.setTotalCount(service.getCount(queryFromJson));
			
			if(filterJson.get("page")!=null) {
				resultListDto.setCurrentPage(Integer.parseInt(filterJson.get("page").toString()));
			}
			
			return resultListDto;
		}

		
		/**
		 * Returns details of {@link Role}} for a given id
		 * 
		 * @param id
		 * @return {@link Role}} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('EMP_VIEW')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ROLE_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping
		public Role read(@RequestParam String id) {
			logger.info("Reading a Role with id: " + id);
			return service.read(id);
		}
		
		/**
		 * Creates new {@link Emp} in the database.
		 * 
		 * @param entity
		 * @return Newly created {@link Role} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_CREATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ROLE_CREATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping
		public Role create(@Valid @RequestBody Role entity) {
			logger.info("Creating a new Role: ");
			return service.create(entity);
		}
	
		/**
		 * Updates an existing {@link Role} in the database.
		 * 
		 * @param entity {@link Role} to update
		 * @return Updated {@link Role} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_UPDATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('ROLE_UPDATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PutMapping
		public Role update(@Valid @RequestBody Role entity) {
			logger.info("Update Role: ");
			return service.update(entity);
		}

        /**
		 * Deletes an existing {@link MarketData} from the database. Delete Multiple at once.
		 * @param objectIds
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('COUNTRY_DELETE')")
		 */
		
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping("/deleteAll")
		public List<ResponseObject> deleteAll(@RequestBody String objectIds[]) {
			
			List<ResponseObject> responseObjectsList = new LinkedList<ResponseObject>();
			for(String id:objectIds)
			{
				ResponseObject responseObject = queryUtil.getResponseObject(id);
				responseObject.setStatus(delete(id));
				
				responseObjectsList.add(responseObject);
			}
			
			return responseObjectsList;
		}
		
		/**
		 * Deletes an existing {@link MarketData} from the database.
		 * 
		 * @param id {@link MarketData} object with the matching id passed in will be deleted..
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('ROLE_DELETE')")
		 */
		//@PreAuthorize("hasAuthority('ROLE_DELETE')")
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping
		public Boolean delete(@RequestParam String id) {
			logger.info("Delete Role: ");
			return service.delete(id);
		}
	}


