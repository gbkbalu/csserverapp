package com.balalatest.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.balalatest.constants.AppConstants;

/**
 *
 * @author
 */
@Controller
@RequestMapping("/uploadFileS3")
public class UploadFileController {

	@Autowired
	private AmazonS3 amazonS3Client;

	@Value("${cloud.aws.s3.bucket}")
	private String bucket;

	private final static String FOLDER_SUFFIX = "/";

	/**
	 * Upload File to S3
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */

	@RequestMapping(value = "/uploadFileToS3", headers = "Content-Type= multipart/form-data", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> uploadFileToS3(@RequestParam("file") MultipartFile file)
			throws IOException {

		Map<String, Object> responseObject = new HashMap<String, Object>();

		try {
			if (!file.isEmpty()) {
				byte[] bytes = file.getBytes();
				Path path = Paths.get(file.getOriginalFilename());
				Files.write(path, bytes);
				File filePath = new File(file.getOriginalFilename());
				file.transferTo(filePath);
				String fileName = AppConstants.FOLDER_NAME + FOLDER_SUFFIX + Calendar.getInstance().getTimeInMillis()
						+ "_" + file.getOriginalFilename();

				amazonS3Client.putObject(new PutObjectRequest(bucket, fileName, filePath)
						.withCannedAcl(CannedAccessControlList.PublicRead));
				String uploadUrl = ((AmazonS3Client) amazonS3Client).getResourceUrl(bucket, fileName);
				responseObject.put("status", HttpStatus.OK);
				responseObject.put("uploadUrl", uploadUrl);

			}
		} catch (AmazonServiceException e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return responseObject;
	}
}


