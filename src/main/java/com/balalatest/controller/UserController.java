package com.balalatest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.balalatest.common.QueryUtil;
import com.balalatest.domain.User;
import com.balalatest.dto.ResultListDto;
import com.balalatest.service.UserBusinessService;
import com.balalatest.validator.UserValidator;
import com.balalatest.common.ResponseObject;
	
	/**
	 * REST controller for {@link User} domain object.
	 * 
	 * 
	 * 
	 * @see User
	 * @see UserBusinessLogic#perform(User, com.balalatest.businessobject.Context)
	 * @see UserValidator#validate(Object, org.springframework.validation.Errors)
	 * @see UserBusinessLogic#perform(java.util.Collection, com.balalatest.businessobject.Context)
	 * @see ResultListDto
	 *
	 * @author Shris Infotech
	 */
	 
	@RestController
	@RequestMapping("/user")
	public class UserController {

		@Autowired
		private UserBusinessService service;
		
		@Autowired
		private QueryUtil queryUtil;
		
		@Autowired
		private UserValidator userValidator;
		
		private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		@InitBinder
		protected void initBinder(WebDataBinder binder) {
	    	binder.setValidator(userValidator);
		}
	
		/**
		 * This method is paginated version of {@link UserController#fetchAllRecords()}
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('USER_VIEW')")
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/records/{page}/{pageSize}")
		public ResultListDto<User> getAllRecords(@PathVariable("page") int page,@PathVariable("pageSize") int pageSize) {
			
			pageSize = queryUtil.getPageSize(pageSize);
	    	final PageRequest pageRequest = new PageRequest(page, pageSize,Sort.Direction.DESC,"updatedOn");
	    	ResultListDto<User> resultListDto = new ResultListDto<User>();
	    	
			// To Do set, right page limit
			Page<User> pageList = service.readAll(pageRequest);
			resultListDto.setResultList(pageList.getContent());
			resultListDto.setPageCount(pageList.getTotalPages());
			resultListDto.setTotalCount(pageList.getTotalElements());
			resultListDto.setCurrentPage(page);
			return resultListDto;
			
		}

		/**
		 * Fetches all the entities of type {@link User} from the data base.
		 * 
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('USER_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping("/fetchAllRecords")
		public ResultListDto<User> fetchAllRecords() {
		
			final List<User> pageList = service.fetchAllRecords();
			final ResultListDto<User> resultListDto = new ResultListDto<User>();
			resultListDto.setResultList(pageList);
			resultListDto.setTotalCount(pageList.size());
			return resultListDto;
		}
		
		/**
		 * Finds list of {@link User} objects matching given request JSON
		 * 
		 * @param filterJson
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_VIEW')")
		 */
		//@PreAuthorize("hasAuthority('USER_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping("/filterByKeys")
		public ResultListDto<User> filterByKeys(@RequestBody JSONObject filterJson) {

			final Query queryFromJson = queryUtil.createQueryFromJson(filterJson);
			final ResultListDto<User> resultListDto = new ResultListDto<User>();
			resultListDto.setResultList(service.filter(queryFromJson));
			resultListDto.setTotalCount(service.getCount(queryFromJson));
			
			if(filterJson.get("page")!=null) {
				resultListDto.setCurrentPage(Integer.parseInt(filterJson.get("page").toString()));
			}
			
			return resultListDto;
		}

		
		/**
		 * Returns details of {@link User}} for a given id
		 * 
		 * @param id
		 * @return {@link User}} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('EMP_VIEW')")
		 *
		 */
		//@PreAuthorize("hasAuthority('USER_VIEW')") 
		@PreAuthorize("hasAuthority('Admin')")
		@GetMapping
		public User read(@RequestParam String id) {
			logger.info("Reading a User with id: " + id);
			return service.read(id);
		}
		
		/**
		 * Creates new {@link Emp} in the database.
		 * 
		 * @param entity
		 * @return Newly created {@link User} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_CREATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('USER_CREATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PostMapping
		public User create(@Valid @RequestBody User entity) {
			logger.info("Creating a new User: ");
			return service.create(entity);
		}
	
		/**
		 * Updates an existing {@link User} in the database.
		 * 
		 * @param entity {@link User} to update
		 * @return Updated {@link User} object
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_UPDATE')")
		 *
		 */
		//@PreAuthorize("hasAuthority('USER_UPDATE')") 
		@PreAuthorize("hasAuthority('Admin')")
		@PutMapping
		public User update(@Valid @RequestBody User entity) {
			logger.info("Update User: ");
			return service.update(entity);
		}

        /**
		 * Deletes an existing {@link MarketData} from the database. Delete Multiple at once.
		 * @param objectIds
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('COUNTRY_DELETE')")
		 */
		
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping("/deleteAll")
		public List<ResponseObject> deleteAll(@RequestBody String objectIds[]) {
			
			List<ResponseObject> responseObjectsList = new LinkedList<ResponseObject>();
			for(String id:objectIds)
			{
				ResponseObject responseObject = queryUtil.getResponseObject(id);
				responseObject.setStatus(delete(id));
				
				responseObjectsList.add(responseObject);
			}
			
			return responseObjectsList;
		}
		
		/**
		 * Deletes an existing {@link MarketData} from the database.
		 * 
		 * @param id {@link MarketData} object with the matching id passed in will be deleted..
		 * @return
		 * 
		 * TO DO : Set Authorization : Ex: @PreAuthorize("hasAuthority('Admin')") or @PreAuthorize("hasRole('USER_DELETE')")
		 */
		//@PreAuthorize("hasAuthority('USER_DELETE')")
		@PreAuthorize("hasAuthority('Admin')")
		@DeleteMapping
		public Boolean delete(@RequestParam String id) {
			logger.info("Delete User: ");
			return service.delete(id);
		}
	}


