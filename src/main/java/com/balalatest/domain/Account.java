package com.balalatest.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import com.balalatest.domain.core.Function;
import com.balalatest.domain.core.Role;
import com.balalatest.domain.core.UserCategory;

public class Account {

	// Something that uniquely identifies each user in the system. This can be
	// emailId.
	@Id
	private String id;
	private String username;
	private String password;
	private List<Role> roles;
	private String enterpriseId;
	private UserCategory category;

	public Account() {
	}

	public Account(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Account(String username, String password, UserCategory category) {
		this.username = username;
		this.password = password;
		this.category = category;
	}

	public Account(String username, String password, List<Role> roles) {
		this.username = username;
		this.password = password;
		this.roles = roles;
	}
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	
	/**
	 * Returns unique set of functions that the user has access to.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Set<Function> getFunctions() {
		final List<Role> rolesTemp = getRoles();

		if (rolesTemp == null) {
			return Collections.EMPTY_SET;
		}
		Set<Function> functions = new HashSet<Function>();

		for (Role role : rolesTemp) {
			if (role.getFunctions() != null && !role.getFunctions().isEmpty()) {
				functions.addAll(role.getFunctions());
			}
		}
		return functions;
	}
	
	/**
	 * Returns unique set of functions in an string array, that the user has access to.
	 * 
	 * @return
	 */
	public String[] getFunctionAsStringArray() {
		
		// Get the functions that user has access to
		final Set<Function> functionsTemp = getFunctions();
		
		// if user does not have access to any functions, return the category the user belongs to.
		if ((functionsTemp == null || functionsTemp.isEmpty()) && category != null) {
			return new String[] { category.name() };
		}
		
		// Add name of all the functions to the set so that only unique ones stays.
		final Set<String> functions = new HashSet<String>();
		for (Function function : functionsTemp) {
			functions.add(function.getName());
		}

		return (String[]) functions.toArray();
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public UserCategory getCategory() {
		return category;
	}

	public void setCategory(UserCategory category) {
		this.category = category;
	}
}
