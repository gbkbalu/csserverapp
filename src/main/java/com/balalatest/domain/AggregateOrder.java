package com.balalatest.domain;

import java.util.Date;
import com.balalatest.domain.core.BaseDomain;


public class AggregateOrder extends BaseDomain{

	private static final long serialVersionUID = 1L;
	private String ticker;
	private String clientId;
	private String accountId;
	private Date orderDate;
	private OrderData orderDataList;
	private int total;
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public OrderData getOrderDataList() {
		return orderDataList;
	}
	public void setOrderDataList(OrderData orderDataList) {
		this.orderDataList = orderDataList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}