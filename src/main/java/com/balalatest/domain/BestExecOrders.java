package com.balalatest.domain;

import java.text.ParseException;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class BestExecOrders {
	
	@Id
	private String id;
	private String orderId;
	private String ticker;
	private String exchange;
	private String bestExchange;
	private double buyPrice;
	private double bestPrice;
	private double prieDifference;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public double getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}

	public double getBestPrice() {
		return bestPrice;
	}

	public void setBestPrice(double bestPrice) {
		this.bestPrice = bestPrice;
	}

	public double getPrieDifference() {
		return prieDifference;
	}

	public void setPrieDifference(double prieDifference) {
		this.prieDifference = prieDifference;
	}
	
	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	
	public String getBestExchange() {
		return bestExchange;
	}

	public void setBestExchange(String bestExchange) {
		this.bestExchange = bestExchange;
	}

	public BestExecOrders()
	{
		
	}
	
	public BestExecOrders(String exchange,String ticker,String orderId, double buyPrice, double bestPrice, String bestExchange) throws ParseException
	{
		this.exchange = exchange;
		this.ticker = ticker;
		this.orderId = orderId;
		this.buyPrice = buyPrice;
		this.bestPrice = bestPrice;
		this.prieDifference = buyPrice- bestPrice;
		this.bestExchange = bestExchange;
	}

	
	
}
