package com.balalatest.domain;
// Imports
import com.balalatest.domain.core.*;
import com.balalatest.domain.core.annotation.*;
import java.util.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
public class Enterprise extends BaseDomain{
		// Field declarations
		// ------------------
		private String enterpriseName;
		private String description;
		private String firstName;
		private String lastName;
		// Getters and Setters
		// ------------------
		public String getEnterpriseName() {
			return this.enterpriseName;
		}
		public void setEnterpriseName(String enterpriseName) {
			this.enterpriseName = enterpriseName;
		}
		public String getDescription() {
			return this.description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getFirstName() {
			return this.firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return this.lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
}
