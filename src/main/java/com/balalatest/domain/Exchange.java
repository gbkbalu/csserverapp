package com.balalatest.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Exchange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5L;
	private String exchange;
	private int count;
	private long totalTimeTaken;
	private long avgTimeTaken;
	private List<String> orderIds;
	
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public long getTotalTimeTaken() {
		return totalTimeTaken;
	}
	public void setTotalTimeTaken(long totalTimeTaken) {
		this.totalTimeTaken = totalTimeTaken;
	}
	public long getAvgTimeTaken() {
		return avgTimeTaken;
	}
	public void setAvgTimeTaken(long avgTimeTaken) {
		this.avgTimeTaken = avgTimeTaken;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<String> getOrderIds() {
		return orderIds;
	}
	public void setOrderIds(List<String> orderIds) {
		this.orderIds = orderIds;
	}
}
