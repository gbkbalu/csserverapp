package com.balalatest.domain;
// Imports
import com.balalatest.domain.core.*;
import com.balalatest.domain.core.annotation.*;
import java.util.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
public class Function extends BaseDomain{
		// Field declarations
		// ------------------
		private String name;
		private String code;
		private String description;
		// Getters and Setters
		// ------------------
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return this.code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDescription() {
			return this.description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
}
