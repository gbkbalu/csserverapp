package com.balalatest.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class MarketData {
	
	private String ticker;
	private double price;
	private String side;
	private String exchange;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date marketDate = new Date();

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public Date getMarketDate() {
		return marketDate;
	}

	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	public MarketData()
	{
		
	}
	
	public MarketData(String ticker,String exchange, String side,String price, String marketDate) throws ParseException
	{
		this.ticker = ticker;
		this.exchange = exchange;
		this.side = side;
		this.price = Double.parseDouble(price);
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		//this.marketDate = formatter.parse(marketDate);
		this.marketDate = new Date();
	}

	
	
}
