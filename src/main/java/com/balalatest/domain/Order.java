package com.balalatest.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import com.balalatest.domain.core.BaseDomain;

@CompoundIndexes({ @CompoundIndex(name = "ticker_client_accountId", def = "{'clientId':1,'accountId':1,'ticker':1}") })
@Document
public class Order extends BaseDomain {

	private static final long serialVersionUID = 1L;
	private String ticker;
	private String clientId;
	private String accountId;
	private Date orderDate;
	@Transient
	private String dateString;
	private ArrayList<OrderData> orderDataList;
	@Transient
	private OrderData orderData;

	private long createdTime = System.currentTimeMillis();

	public Order() {
	}

	public ArrayList<OrderData> getOrderDataList() {
		return orderDataList;
	}

	public void setOrderDataList(List<OrderData> orderDataList) {
		this.orderDataList = new ArrayList<>(orderDataList);
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getJsonFromOrder(Order object) throws JsonMappingException, JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(object);
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public OrderData getOrderData() {
		return orderData;
	}

	public void setOrderData(OrderData orderData) {
		this.orderData = orderData;
	}

	public void setOrderDataList(ArrayList<OrderData> orderDataList) {
		this.orderDataList = orderDataList;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	

}