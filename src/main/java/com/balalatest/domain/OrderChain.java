package com.balalatest.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * 
 * Sample Order:
 * 
 * {
    "_id" : "123", // Root order
    "orderSet" : [ 
        "123", 
        "456", 
        "789"
    ],
    "currentStatus" : "s6",
    "orderChain" : [ 
        {
            "orderId" : "123",
            "status" : "s1"
        }, 
        {
            "orderId" : "456",
            "status" : "s2"
        }, 
        {
            "orderId" : "456",
            "status" : "s3"
        }, 
        {
            "orderId" : "456",
            "status" : "s4"
        }, 
        {
            "orderId" : "789",
            "status" : "s5"
        }, 
        {
            "orderId" : "789",
            "status" : "s6"
        }
    ]
}
 * 
 * @author Shris Infotech
 *
 */
public class OrderChain {
	
	@Id
	private String rootOrderId; //Root order id
	@Indexed(unique=true)
	private String[] orderSet;
	private String tag35;
	private String tag39;
	private List<OrderData> orderChain;
	
	
	public String getRootOrderId() {
		return rootOrderId;
	}
	public void setRootOrderId(String rootOrderId) {
		this.rootOrderId = rootOrderId;
	}
	public String[] getOrderSet() {
		return orderSet;
	}
	public void setOrderSet(String[] orderSet) {
		this.orderSet = orderSet;
	}
	
	public String getTag35() {
		return tag35;
	}
	public void setTag35(String tag35) {
		this.tag35 = tag35;
	}
	public String getTag39() {
		return tag39;
	}
	public void setTag39(String tag39) {
		this.tag39 = tag39;
	}
	public List<OrderData> getOrderChain() {
		return orderChain;
	}
	public void setOrderChain(List<OrderData> orderChain) {
		this.orderChain = orderChain;
	}
	
	
}
