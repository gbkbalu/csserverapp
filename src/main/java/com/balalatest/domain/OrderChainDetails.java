package com.balalatest.domain;

import java.util.Date;

public class OrderChainDetails {
	
	public String orderId;
	public String tag35;
	public String tag39;
	public Date orderDate;
	public int qty;
	
	public OrderChainDetails(OrderData orderData) {
		this.orderId = orderData.getOrderId();
		this.orderDate = orderData.getOrderDate();
		this.qty = orderData.getQty();
		this.tag35=orderData.getTagValues().get("tag35");
		this.tag39=orderData.getTagValues().get("tag39");
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTag35() {
		return tag35;
	}
	public void setTag35(String tag35) {
		this.tag35 = tag35;
	}
	public String getTag39() {
		return tag39;
	}
	public void setTag39(String tag39) {
		this.tag39 = tag39;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	
}
