package com.balalatest.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class SparkOrderData {
	
	@Id
	@Indexed
	private String orderId;
	private int qty;
	@Transient
	private String ticker;
	@Transient
	private String clientId;
	@Transient
	private String accountId;
	private String notionalValue;
	private String side;
	private String msg;
	@Transient
	private String dateString;
	private Map<String, String> tagValues = new HashMap<String, String>();
	private String parentOrder;
	private List<String> descendentOrders;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Timestamp orderDate;
	

	private String orderTime;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getNotionalValue() {
		return notionalValue;
	}

	public void setNotionalValue(String notionalValue) {
		this.notionalValue = notionalValue;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	
	
	public Timestamp getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = new Timestamp(orderDate.getTime());
	}
	
	public void setOrderDate(long time) {
		this.orderDate = new Timestamp(time);
	}
	
	public Map<String, String> getTagValues() {
		return tagValues;
	}

	public void setTagValues(Map<String, String> tagValues) {
		this.tagValues = tagValues;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public SparkOrderData() {

	}

	public String getParentOrder() {
		return parentOrder;
	}

	public void setParentOrder(String parentOrder) {
		this.parentOrder = parentOrder;
	}

	public List<String> getDescendentOrders() {
		return descendentOrders;
	}

	public void setDescendentOrders(List<String> descendentOrders) {
		this.descendentOrders = descendentOrders;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
	
}
