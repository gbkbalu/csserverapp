package com.balalatest.domain;

import com.balalatest.domain.core.BaseDomain;


public class TotalCount extends BaseDomain{

	private static final long serialVersionUID = 1L;
	private int total;
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}