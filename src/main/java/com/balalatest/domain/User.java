package com.balalatest.domain;
// Imports
import com.balalatest.domain.core.*;
import com.balalatest.domain.core.annotation.*;
import java.util.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
public class User extends BaseDomain{
		// Field declarations
		// ------------------
		private String userName;
		private String firstName;
		private String lastName;
		private String enterpriseId;
		// Getters and Setters
		// ------------------
		public String getUserName() {
			return this.userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getFirstName() {
			return this.firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return this.lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEnterpriseId() {
			return this.enterpriseId;
		}
		public void setEnterpriseId(String enterpriseId) {
			this.enterpriseId = enterpriseId;
		}
}
