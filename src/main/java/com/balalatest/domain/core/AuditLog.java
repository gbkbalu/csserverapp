package com.balalatest.domain.core;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Domain object representing audit logs.
 * 
 * @author Shris Infotech
 *
 */
@Document
public class AuditLog {
	
	@Id
	private String id;
	// class name of the source domain object. Ex: com.shris.domain.Emp
	private String source;
	// Value of the id field.
	private String sourceId;
	// Who modified
	private String modifiedBy;
	// When was it modified
	private Date modifiedDate;
	// Previous data
	private String previousData;

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPreviousData() {
		return previousData;
	}

	public void setPreviousData(String previousData) {
		this.previousData = previousData;
	}

	
	
}
