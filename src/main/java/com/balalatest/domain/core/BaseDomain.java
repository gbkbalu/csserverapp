package com.balalatest.domain.core;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base class for any domain object that requires an id field and generic getProperty and setProperty methods.
 * 
 * @author Shris Infotech
 *
 */
public abstract class BaseDomain extends AbstractAuditableEntity {
	
	@Id
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
	@JsonIgnore
	public boolean isNew() {
		return (id == null || id.equals(""));
	}
	
	/**
	 * Retrieves value of a property/field defined on the domain object.
	 * 
	 * Ex: If empName is the name of the field calling getProperty("emapName") returns the employee name.
	 * 
	 * @param propertyName Name of the the property/field on the domain object.
	 * @return Value of the domain object for the property
	 * 
	 * @throws Exception When value could not be retrieved in a reflective way.
	 */
	public Object getProperty(String propertyName) throws Exception {
		return BeanUtils.getProperty(this, propertyName);
	}
	
	/**
	 * Sets value to a property/field defined on the domain object.
	 * 
	 * @param propertyName Name of the the property/field on the domain object.
	 * @param value Value to be set
	 * 
	 * @throws Exception When value could not be set in a reflective way.
	 */
	public void setProperty(String propertyName, Object value) throws Exception{
		BeanUtils.setProperty(this, propertyName, value);
	}
}
