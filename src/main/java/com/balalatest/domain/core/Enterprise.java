package com.balalatest.domain.core;

import java.util.List;

/**
 * Enterprise is any client who owns an account in the system in a SAAS model.
 * 
 * @author Shris Infotech.
 *
 */
public class Enterprise extends SoftDeleteBaseDomain{
	
	private String name;
	private String emailId;
	private String address;
	private List<Module> modules;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<Module> getModules() {
		return modules;
	}
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
}
