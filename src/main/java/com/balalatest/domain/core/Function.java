package com.balalatest.domain.core;

import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Domain object that represents Function under a module. 
 * 
 * Ex: If Shopping Cart is a module, Payment is a module then CRUD operations on these modules are the functions.
 * 
 * @author Shris Infotech.
 * @see Module
 */
public class Function extends BaseDomain{
	private String name;
	private String displayName;
	@DBRef
	private Module module;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Module getModule() {
		return module;
	}
	public void setModule(Module module) {
		this.module = module;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Function) {
			Function dest = (Function) obj;
			if(dest.getId() == this.getId()) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.getName();
	}
}
