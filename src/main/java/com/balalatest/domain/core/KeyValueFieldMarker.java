package com.balalatest.domain.core;

/** 
 * This marker gives out certain meta information about the Domaion object.
 * 
 * @author Shris Infotech.
 *
 */
public interface KeyValueFieldMarker {
	
	/**
	 * Name of the filed the uniquely identifies a given domain object. Ex: If empId is name of the id field of an Emp domain object. This method returns "empId"
	 * 
	 * @return Name of the id field.
	 */
	 public String idField();
	 
	 /**
	  * Name of the filed the is used to identify the domain object. Ex: employeeName for an Emp object. countryName for a Country object.
	  * 
	  * @return Name of the Id filed.
	  */
	 public String nameField();
	 /**
	  * Value of the Id filed.
	  * 
	  * @return Value of the Id filed.
	  */
	 public Object getId();
	 /**
	  * Value of the display filed.
	  * 
	  * @return Value of the display filed.
	  */
	 public String getName();
	 /**
	  * Set value for Id filed.
	  * @param id
	  */
	 public void setId(Object id);
	 
	 /**
	  * Set value for name filed.
	  * 
	  * @param name
	  */
	 public void setName(String name);
}
