package com.balalatest.domain.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Domain object that represents module in an application. Ex: Shopping Cart is a module, Payment is a module.
 * 
 * @author Shris Infotech.
 * @see Function
 */
public class Module extends BaseDomain{
	
	private String name;
	private String displayName;
	@DBRef
	private List<Function> functions = new ArrayList<Function>();
	@DBRef
	private List<Module> ancestors;
	@DBRef
	private Module parent;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Function> getFunctions() {
		return functions;
	}
	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}
	public List<Module> getAncestors() {
		return ancestors;
	}
	public void setAncestors(List<Module> ancestors) {
		this.ancestors = ancestors;
	}
	public Module getParent() {
		return parent;
	}
	public void setParent(Module parent) {
		this.parent = parent;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
}
