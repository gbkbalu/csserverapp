package com.balalatest.domain.core;

public enum Operator {
	Equals, NotEquals, GreatherThan, LessThan, Empty, NotEmpty, LessThanOrEqual, GreatherThanOrEqual, Approximate, Contains
}
