package com.balalatest.domain.core;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;

import com.balalatest.domain.core.annotation.Required;
public class Role extends BaseDomain{
		// Field declarations
		// ------------------
		@Required
		private String name;
		@DBRef
		private List<Function> functions;
		// Getters and Setters
		// ------------------
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<Function> getFunctions() {
			return this.functions;
		}
		public void setFunctions(List<Function> functions) {
			this.functions = functions;
		}
}
