package com.balalatest.domain.core;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base class for any domain object that requires to be soft deleted.
 * 
 * @author Shris Infotech
 *
 */
public abstract class SoftDeleteBaseDomain extends BaseDomain {
	
	@JsonIgnore
	private boolean deleteFlag = false;

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	
}
