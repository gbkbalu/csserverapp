package com.balalatest.domain.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)

/**
 * Type annotated with this indicates that the source object referencing the type can cascade the save.
 * In another words this is used to implement "Put If Does Not Exist" pattern
 * 
 * Ex: If Emp holds reference to a Department, and Department field in Emp is annotated with CascadeSave, the save to Emp will trigger a save to Department if it does not exist already.
 * 
 * @author Shris Infotech
 *
 */
public @interface CascadeSave {}