package com.balalatest.domain.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation indicates that the type needs to be soft deleted. i.e. When a
 * delete happens, record stays in the data base but a flag is used to indicate
 * that it has been deleted.
 * 
 * @author Shris Infotech
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SoftDelete {
}