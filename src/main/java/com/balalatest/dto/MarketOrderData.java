package com.balalatest.dto;

import java.util.List;
import com.balalatest.domain.AggregateOrder;
import com.balalatest.domain.LogStashRecord;
import com.balalatest.domain.MarketData;

/**
 * Used as data transfer object for all the DB operations.
 * 
 * 
 * @param <T>
 *
 * @author Shris Infotech
 */
 
public class MarketOrderData {


	private AggregateOrder aggregateOrder;
	private List<MarketData>  marketDataList;
	private List<LogStashRecord>  logStashRecordsList;
	
	public AggregateOrder getAggregateOrder() {
		return aggregateOrder;
	}
	public void setAggregateOrder(AggregateOrder aggregateOrder) {
		this.aggregateOrder = aggregateOrder;
	}
	public List<MarketData> getMarketDataList() {
		return marketDataList;
	}
	public void setMarketDataList(List<MarketData> marketDataList) {
		this.marketDataList = marketDataList;
	}
	public List<LogStashRecord> getLogStashRecordsList() {
		return logStashRecordsList;
	}
	public void setLogStashRecordsList(List<LogStashRecord> logStashRecordsList) {
		this.logStashRecordsList = logStashRecordsList;
	}
	
}
