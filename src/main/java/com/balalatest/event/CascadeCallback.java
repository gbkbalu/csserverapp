package com.balalatest.event;

import java.lang.reflect.Field;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.util.ReflectionUtils;

import com.balalatest.domain.core.BaseDomain;
import com.balalatest.domain.core.annotation.CascadeSave;

/**
 * Used in Cascading infrastructure. 
 * 
 * @author Shris Infotech.
 * @see CascadeSaveMongoEventListener
 */
public class CascadeCallback implements ReflectionUtils.FieldCallback {

    private Object source;
    private MongoOperations mongoOperations;

    public CascadeCallback(final Object source, final MongoOperations mongoOperations) {
        this.source = source;
        this.setMongoOperations(mongoOperations);
    }

    @Override
    public void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException {
        ReflectionUtils.makeAccessible(field);
        
        // Save only when the field is annotated with both DBRef and CascadeSave and the entity is a new entity. i.e Does not already exist in persistent store.
        if (field.isAnnotationPresent(DBRef.class) && field.isAnnotationPresent(CascadeSave.class)) {
            final Object fieldValue = field.get(getSource());

            if (fieldValue != null) {
                final FieldCallback callback = new FieldCallback();

                ReflectionUtils.doWithFields(fieldValue.getClass(), callback);
                
                if(fieldValue instanceof BaseDomain)  {
                	BaseDomain baseDomain = (BaseDomain)fieldValue;
                	if(baseDomain.isNew()) {
                		getMongoOperations().save(fieldValue);
                	}
                }
                
            }
        }
    }

    public Object getSource() {
        return source;
    }

    public MongoOperations getMongoOperations() {
        return mongoOperations;
    }

    public void setMongoOperations(final MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }
}

