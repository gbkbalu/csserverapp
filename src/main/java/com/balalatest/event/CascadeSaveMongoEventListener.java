package com.balalatest.event;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.balalatest.domain.core.AuditLog;
import com.balalatest.domain.core.BaseDomain;
import com.balalatest.domain.core.annotation.Auditable;


/**
 * Listener of Mongo onBeforeConvert and onAfterSave events.
 * 
 * Cascades saves and does an entry to audit log table based on how a domain model is annotated.
 * 
 * @author Shris Infotech
 *
 */
@Configuration
public class CascadeSaveMongoEventListener extends AbstractMongoEventListener<Object> {

	@Autowired
	private MongoOperations mongoOperations;
	private String priorData = null;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	//@Override
	public void onBeforeConvert(final Object source) {
		// Get the source object from DB with the previous values BaseDomain
		// Deal with auditing.
		dealWithAuditing(source);
		// Deal with cascade save of the fields.
		ReflectionUtils.doWithFields(source.getClass(), new CascadeCallback(source, mongoOperations));
	}

	private void dealWithAuditing(Object source) {
		// Initialize priorData
		priorData = null;

		// Check if the domain object that we are trying to save is annotated
		// with Auditable.
		// If Yes, the the previous value for the fields needs to be saved for a
		// later comparison.
		if (source.getClass().isAnnotationPresent(Auditable.class) && source instanceof BaseDomain) {

			BaseDomain base = (BaseDomain) source;
			Object temp = mongoOperations.findById(base.getId(), source.getClass());
			ObjectMapper mapper = new ObjectMapper();
			try {
				priorData = mapper.writeValueAsString(temp);
			} catch (JsonProcessingException e) {
				// Do nothing
				logger.debug(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onAfterSave(AfterSaveEvent<Object> event) {
		
		// TO DO: Consider moving this to separate Thread/ Work Queue.
		super.onAfterSave(event);
		// Deal with stuff like auditing here.
		Object source = event.getSource();

		if (source != null && priorData != null && source.getClass().isAnnotationPresent(Auditable.class)) {
			AuditLog auditLog = new AuditLog();
			auditLog.setSource(source.getClass().getName());
			if (source instanceof BaseDomain) {
				if (auditLog.getId() == null) {
					auditLog.setId(UUID.randomUUID().toString());
				}
				auditLog.setSourceId(((BaseDomain) source).getId());
				auditLog.setModifiedDate(((BaseDomain) source).getModifiedDate());
				auditLog.setModifiedBy(((BaseDomain) source).getModifiedBy());
				auditLog.setPreviousData(priorData);
			}
			mongoOperations.save(auditLog);
		}

	}
}
