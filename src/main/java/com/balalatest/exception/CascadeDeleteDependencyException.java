package com.balalatest.exception;

/**
 * Exception that is thrown when delete of a master domain object cannot be done
 * as child domain objects has dependency on it.
 * 
 * Ex: If Employee domain object has dependency on Department domain object, and
 * when delete on a dependent Department is triggered, this exception is thrown
 * if EmployeeBusinessService.
 * 
 * @author Shris Infotech
 *
 */
public class CascadeDeleteDependencyException extends RuntimeException {

	/**
	 * Adding serial Version UID
	 */
	private static final long serialVersionUID = 5874672668641468579L;

	public CascadeDeleteDependencyException(String message) {
		super(message);
	}

}
