package com.balalatest.generic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Provides methods to send email. Makes use of properties configured in application.properties to configure {@link JavaMailSender}.
 * 
 * @author Shris Infotech
 *
 */
@Service
public class MailSenderService {

	@Autowired
	JavaMailSender javaMailSender;
	
	/**
	 * Sends email synchronously. (Blocking I/O)
	 * 
	 * @param mailMessage
	 */
	public void send(final SimpleMailMessage mailMessage) {
		javaMailSender.send(mailMessage);
	}
	
	/**
	 * Sends email asynchronously. (Non-Blocking I/O)
	 * 
	 * @param mailMessage
	 */
	public void sendAsync(final SimpleMailMessage mailMessage) {
		Runnable runnable = new Runnable() {
			public void run() {
				send(mailMessage);
			}
		};
		new Thread(runnable).start();
	}
}
