package com.balalatest.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.balalatest.domain.Account;

public interface AccountRepository extends MongoRepository<Account, String> {
  public Account findByUsername(String username);
}