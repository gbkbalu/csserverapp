package com.balalatest.repository;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.BestExecOrders;
/**
 * Repository for {@link BestExecOrders} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface BestExecOrdersRepository extends MongoRepository<BestExecOrders, String> {
    @Query("{'_id':{$eq: ?0}}")
	Optional<BestExecOrders> findById(String id);
	BestExecOrders findByOrderId(String orderId);
	BestExecOrders findByTicker(String ticker);
	BestExecOrders findByExchange(String exchange);
	BestExecOrders findByBuyPrice(double buyPrice);
	BestExecOrders findByBestPrice(double bestPrice);
	BestExecOrders findByPrieDifference(double prieDifference);
}
