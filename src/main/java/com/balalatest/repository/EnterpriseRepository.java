package com.balalatest.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.*;
import java.util.*;
import java.util.Optional;
/**
 * Repository for {@link Enterprise} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface EnterpriseRepository extends MongoRepository<Enterprise, String> {
	Optional<Enterprise> findById(String id);
	Enterprise findByEnterpriseName(String enterpriseName);
	Enterprise findByDescription(String description);
	Enterprise findByFirstName(String firstName);
	Enterprise findByLastName(String lastName);
}
