package com.balalatest.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.Exchange;
import com.balalatest.domain.Order;
/**
 * Repository for {@link Order} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface ExchangeRepository extends MongoRepository<Exchange, String> {
    
}
