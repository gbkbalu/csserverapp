package com.balalatest.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.*;
import java.util.*;
import java.util.Optional;
/**
 * Repository for {@link Function} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface FunctionRepository extends MongoRepository<Function, String> {
	Optional<Function> findById(String id);
	Function findByName(String name);
	Function findByCode(String code);
	Function findByDescription(String description);
}
