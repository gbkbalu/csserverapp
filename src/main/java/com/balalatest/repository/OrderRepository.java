package com.balalatest.repository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.Order;
import com.balalatest.domain.OrderData;
/**
 * Repository for {@link Order} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface OrderRepository extends MongoRepository<Order, String> {
    @Query("{'_id':{$eq: ?0}}")
	Optional<Order> findById(String id);
	Order findByTicker(String ticker);
	Order findByClientId(String clientId);
	Order findByAccountId(String accountId);
	Order findByOrderDate(Date orderDate);
	List<Order> findByOrderDataList(OrderData orderDataList);
}
