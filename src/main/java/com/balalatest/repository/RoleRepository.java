package com.balalatest.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.*;
import java.util.*;
import java.util.Optional;
/**
 * Repository for {@link Role} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
	Optional<Role> findById(String id);
	Role findByName(String name);
	Role findByDescription(String description);
}
