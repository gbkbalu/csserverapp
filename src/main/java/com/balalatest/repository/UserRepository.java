package com.balalatest.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.balalatest.domain.*;
import java.util.*;
import java.util.Optional;
/**
 * Repository for {@link User} domain object.
 *
 * @author Shris Infotech
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
	Optional<User> findById(String id);
	User findByUserName(String userName);
	User findByFirstName(String firstName);
	User findByLastName(String lastName);
	User findByEnterpriseId(String enterpriseId);
}
