package com.balalatest.security;

import java.util.LinkedList;

public class ClientUser {

	private String id;
	private String firstName;
	private String lastName;
	private String userName;
	private String enterpriseId;
	private LinkedList<String> authList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public LinkedList<String> getAuthList() {
		return authList;
	}

	public void setAuthList(LinkedList<String> authList) {
		this.authList = authList;
	}

}
