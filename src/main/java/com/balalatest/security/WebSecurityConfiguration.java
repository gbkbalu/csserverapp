package com.balalatest.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.balalatest.domain.Account;
import com.balalatest.domain.core.Role;
import com.balalatest.repository.AccountRepository;

@Configuration
//@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	AccountRepository accountRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Bean
	UserDetailsService userDetailsService() {
		return new UserDetailsService() 
		{

			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				Account account = accountRepository.findByUsername(username);
				if (account != null) {

					UserPrincipal principal = new UserPrincipal(account.getUsername(), account.getPassword(), enabled, accountNonExpired,
							credentialsNonExpired, accountNonLocked, getAuthorities(account));
					
					ClientUser clientUser = new ClientUser();
					if(account != null)
					{
						clientUser.setUserName(account.getUsername());
						clientUser.setId(account.getId());
						
						Collection<? extends GrantedAuthority> authorityList = getAuthorities(account);
						
						LinkedList<String> authList = new LinkedList<String>();
						if(authorityList != null && authorityList.size()>0)
						{
							Iterator itr = authorityList.iterator();
							
							while(itr.hasNext()) 
							{
								GrantedAuthority authority = (GrantedAuthority) itr.next();
								authList.add(authority.getAuthority());
							}
						}
						
						clientUser.setAuthList(authList);
					}
					
					principal.setClientUser(clientUser);
					
					return principal;
				} else 
				{
					throw new UsernameNotFoundException("could not find the user '" + username + "'");
				}
			}

		};
	}
	
	public List<GrantedAuthority> getAuthorities(Account account) {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		
		if(account != null && account.getCategory() != null)
		{
			authList.add(new SimpleGrantedAuthority(account.getCategory().toString()));
		}
		
		System.out.println("HI>>>>>::"+account.getCategory().toString());
		
		/*if(account != null && account.getRoles() != null && !account.getRoles().isEmpty())
		{
			for(Role role:account.getRoles())
			{
				authList.add(new SimpleGrantedAuthority(role.getName()));
			}
		}*/
		
		return authList;
	}
}

@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private UserDetailsService userDetailsService;
	
	@Autowired
    private AuthFailureHandler authFailureHandler;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().authorizeRequests().antMatchers("/loginFailure","/logout","/accessdenied","/","/*.html","/js/**","/css/**","/images/**","/loginFailure", "/accessdenied", "/isLoggedIn","/loginCall", "/", "/*", "/**").permitAll()
		.anyRequest().fullyAuthenticated().and().httpBasic()
		.and()
		.formLogin().usernameParameter("username").passwordParameter("password")
		.loginPage("/accessdenied").loginProcessingUrl("/login").defaultSuccessUrl("/getAuthoritiesList").failureHandler(authFailureHandler).permitAll()
        .and()
        .logout().logoutUrl("/logoutSuccess").deleteCookies("JSESSIONID").permitAll()
        .and()
        .csrf().disable();
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
}

@Component
class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler{

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        System.out.println("AuthFailureHandler.onAuthenticationFailure()");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        redirectStrategy.sendRedirect(request, response, "/loginFailure");
    }   
}