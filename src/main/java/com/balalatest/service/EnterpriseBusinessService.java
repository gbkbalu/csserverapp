package com.balalatest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Enterprise;
import com.balalatest.businessobject.EnterpriseBusinessLogic;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Enterprise}
 * 
 * 
 * @see EnterpriseRepository
 * @see EnterpriseBusinessLogic#perform(MarketData, Context)
 * @see EnterpriseBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
//@CacheConfig(cacheNames = "Enterprises")
public class EnterpriseBusinessService extends GenericBusinessService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EnterpriseDataService enterpriseDataService;
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	final EnterpriseBusinessLogic enterpriseBusinessLogic = new EnterpriseBusinessLogic();
	
	
	
	/**
	 * This method is paginated version of {@link EnterpriseService#fetchAllRecords()}
	 * 
	 * @param page
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */	
	public Page<Enterprise> readAll(final PageRequest page) {
		final Page<Enterprise> entities = enterpriseDataService.readAll(page);
		enterpriseBusinessLogic.perform(entities.getContent(), Context.GET);
		return entities;
	}
	
	/**
	 * Fetches all the entities of type {@link Enterprise} from the data base.
	 * 
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */
	public List<Enterprise> fetchAllRecords() {
		final List<Enterprise> entities = enterpriseDataService.fetchAllRecords();
		enterpriseBusinessLogic.perform(entities, Context.GET);
		return entities;
	}
	
	/**
	 *
     * Finds list of {@link Enterprise} objects matching given query.
     * 
	 * @param query
	 * @return
	 */
	public List<Enterprise> filter(Query query) {
		final List<Enterprise> entities = enterpriseDataService.filter(query);
		enterpriseBusinessLogic.perform(entities, Context.FIND);
		return entities;
	}
	
	/**
	 * Returns count of {@link Enterprise} objects matching a given query.
	 * 
	 * @param query
	 * @return count
	 */
	public Long getCount(Query query) {
		return enterpriseDataService.getCount(query);
	}
	
	/**
	 *
     *  Returns details of {@link Enterprise} object for a given id.
     * 
	 * @param emp
	 * @return
	 */
	public Enterprise read(String id) {
		Enterprise entity = new Enterprise();
		entity.setId(id);
		enterpriseBusinessLogic.perform(entity, Context.GET);
		return enterpriseDataService.read(entity);
	}
	
	/**
	 * Executes business logic on {@link Enterprise} and persists it to the DB.
	 * 
	 * @param entity
	 *            Entity to persist in the DB
	 * @return newly created {@link Enterprise}
	 * 
	 *  TO DO: Add @CachePut to the method if the object needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 */
	public Enterprise create(Enterprise entity) {
		enterpriseBusinessLogic.perform(entity, Context.CREATE);
		return enterpriseDataService.save(entity);
	}
	
	/**
	 * 
	 * Updates an existing {@link Enterprise} in the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Enterprise} is cached, update the cache as well.
	 */
	public Enterprise update(Enterprise entity) {
		Enterprise existingEntity = enterpriseDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot update Enterprise. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot update Enterprise. No entity with id " + entity.getId() + " exists");
		}
		enterpriseBusinessLogic.perform(entity, Context.UPDATE);
		return enterpriseDataService.save(entity);
	}
	
	/**
	 * 
	 * Deletes an existing {@link Enterprise} from the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Enterprise} is cached, delete from cache as well.
	 * Ex: @CacheEvict
	 */
	public Boolean delete(String id) {
	    Enterprise entity = new Enterprise();
	    entity.setId(id);
		Enterprise existingEntity = enterpriseDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot delete Enterprise. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot delete Enterprise. No entity with id " + entity.getId() + " exists");
		}
		super.preDelete(entity);
		enterpriseBusinessLogic.perform(entity, Context.DELETE);
		enterpriseDataService.delete(existingEntity);
		super.postDelete(entity);
		return true;
	}
}
