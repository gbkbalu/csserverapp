package com.balalatest.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Enterprise;
import com.balalatest.repository.EnterpriseRepository;
import com.balalatest.businessobject.EnterpriseBusinessLogic;
import com.balalatest.businessobject.EnterpriseDataEnricher;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Enterprise}
 * 
 * 
 * @see EnterpriseRepository
 * @see EnterpriseBusinessLogic#perform(Enterprise, Context)
 * @see EnterpriseBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
public class EnterpriseDataService extends GenericDataService{

	@Autowired
	private EnterpriseRepository entityRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private EnterpriseDataEnricher enterpriseDataEnricher = new EnterpriseDataEnricher();
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Saves Enterprise to persistence store.
	 */
	public Enterprise save(Enterprise entity) {
		final Enterprise enterprise = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(enterprise);
		return enterprise;
	}
	
	/**
	 * Finds Enterprise with a given id from persistence store.
	 */
	public Enterprise findById(final String id) {
		final Enterprise enterprise = entityRepository.findById(id).get();
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(enterprise);
		return enterprise;
	}
	
	/**
	 * Reads all the Enterprise entities from the persistence store with the size limit matching the page request.
	 */
	public Page<Enterprise> readAll(final PageRequest page) {
		final Page<Enterprise> entities = entityRepository.findAll(page);
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads all the Enterprise entities from the persistence store.
	 */
	public List<Enterprise> fetchAllRecords() {
		final List<Enterprise> entities = entityRepository.findAll();
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads Enterprise entities from the persistence store matching a give query.
	 */
	public List<Enterprise> filter(Query query) {
		final List<Enterprise> entities = mongoTemplate.find(query, Enterprise.class);
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Returns count of Enterprise entities from the persistence store.
	 */
	public Long getCount(Query query) {
		return mongoTemplate.count(query,Enterprise.class);
	}
	
	/**
	 * Reads Enterprise from persistence store for a given id.
	 * 
	 * @param entity
	 * @return
	 */
	public Enterprise read(Enterprise entity) {
		final Enterprise enterprise =  entityRepository.findById(entity.getId()).get();
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(enterprise);
		return enterprise;
	}
	
	/**
	 * Updates Enterprise in persistence store.
	 */
	public Enterprise update(Enterprise entity) {
		final Enterprise enterprise = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		enterpriseDataEnricher.enrich(enterprise);
		return enterprise;
	}
	
	/**
	 * Deletes Enterprise in persistence store.
	 */
	public Boolean delete(Enterprise entity) {
		entityRepository.delete(entity);
		return true;
	}
}
