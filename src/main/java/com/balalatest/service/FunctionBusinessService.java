package com.balalatest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Function;
import com.balalatest.businessobject.FunctionBusinessLogic;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Function}
 * 
 * 
 * @see FunctionRepository
 * @see FunctionBusinessLogic#perform(MarketData, Context)
 * @see FunctionBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
//@CacheConfig(cacheNames = "Functions")
public class FunctionBusinessService extends GenericBusinessService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	FunctionDataService functionDataService;
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	final FunctionBusinessLogic functionBusinessLogic = new FunctionBusinessLogic();
	
	
	
	/**
	 * This method is paginated version of {@link FunctionService#fetchAllRecords()}
	 * 
	 * @param page
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */	
	public Page<Function> readAll(final PageRequest page) {
		final Page<Function> entities = functionDataService.readAll(page);
		functionBusinessLogic.perform(entities.getContent(), Context.GET);
		return entities;
	}
	
	/**
	 * Fetches all the entities of type {@link Function} from the data base.
	 * 
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */
	public List<Function> fetchAllRecords() {
		final List<Function> entities = functionDataService.fetchAllRecords();
		functionBusinessLogic.perform(entities, Context.GET);
		return entities;
	}
	
	/**
	 *
     * Finds list of {@link Function} objects matching given query.
     * 
	 * @param query
	 * @return
	 */
	public List<Function> filter(Query query) {
		final List<Function> entities = functionDataService.filter(query);
		functionBusinessLogic.perform(entities, Context.FIND);
		return entities;
	}
	
	/**
	 * Returns count of {@link Function} objects matching a given query.
	 * 
	 * @param query
	 * @return count
	 */
	public Long getCount(Query query) {
		return functionDataService.getCount(query);
	}
	
	/**
	 *
     *  Returns details of {@link Function} object for a given id.
     * 
	 * @param emp
	 * @return
	 */
	public Function read(String id) {
		Function entity = new Function();
		entity.setId(id);
		functionBusinessLogic.perform(entity, Context.GET);
		return functionDataService.read(entity);
	}
	
	/**
	 * Executes business logic on {@link Function} and persists it to the DB.
	 * 
	 * @param entity
	 *            Entity to persist in the DB
	 * @return newly created {@link Function}
	 * 
	 *  TO DO: Add @CachePut to the method if the object needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 */
	public Function create(Function entity) {
		functionBusinessLogic.perform(entity, Context.CREATE);
		return functionDataService.save(entity);
	}
	
	/**
	 * 
	 * Updates an existing {@link Function} in the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Function} is cached, update the cache as well.
	 */
	public Function update(Function entity) {
		Function existingEntity = functionDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot update Function. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot update Function. No entity with id " + entity.getId() + " exists");
		}
		functionBusinessLogic.perform(entity, Context.UPDATE);
		return functionDataService.save(entity);
	}
	
	/**
	 * 
	 * Deletes an existing {@link Function} from the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Function} is cached, delete from cache as well.
	 * Ex: @CacheEvict
	 */
	public Boolean delete(String id) {
	    Function entity = new Function();
	    entity.setId(id);
		Function existingEntity = functionDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot delete Function. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot delete Function. No entity with id " + entity.getId() + " exists");
		}
		super.preDelete(entity);
		functionBusinessLogic.perform(entity, Context.DELETE);
		functionDataService.delete(existingEntity);
		super.postDelete(entity);
		return true;
	}
}
