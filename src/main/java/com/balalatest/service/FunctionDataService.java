package com.balalatest.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Function;
import com.balalatest.repository.FunctionRepository;
import com.balalatest.businessobject.FunctionBusinessLogic;
import com.balalatest.businessobject.FunctionDataEnricher;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Function}
 * 
 * 
 * @see FunctionRepository
 * @see FunctionBusinessLogic#perform(Function, Context)
 * @see FunctionBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
public class FunctionDataService extends GenericDataService{

	@Autowired
	private FunctionRepository entityRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private FunctionDataEnricher functionDataEnricher = new FunctionDataEnricher();
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Saves Function to persistence store.
	 */
	public Function save(Function entity) {
		final Function function = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(function);
		return function;
	}
	
	/**
	 * Finds Function with a given id from persistence store.
	 */
	public Function findById(final String id) {
		final Function function = entityRepository.findById(id).get();
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(function);
		return function;
	}
	
	/**
	 * Reads all the Function entities from the persistence store with the size limit matching the page request.
	 */
	public Page<Function> readAll(final PageRequest page) {
		final Page<Function> entities = entityRepository.findAll(page);
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads all the Function entities from the persistence store.
	 */
	public List<Function> fetchAllRecords() {
		final List<Function> entities = entityRepository.findAll();
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads Function entities from the persistence store matching a give query.
	 */
	public List<Function> filter(Query query) {
		final List<Function> entities = mongoTemplate.find(query, Function.class);
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Returns count of Function entities from the persistence store.
	 */
	public Long getCount(Query query) {
		return mongoTemplate.count(query,Function.class);
	}
	
	/**
	 * Reads Function from persistence store for a given id.
	 * 
	 * @param entity
	 * @return
	 */
	public Function read(Function entity) {
		final Function function =  entityRepository.findById(entity.getId()).get();
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(function);
		return function;
	}
	
	/**
	 * Updates Function in persistence store.
	 */
	public Function update(Function entity) {
		final Function function = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		functionDataEnricher.enrich(function);
		return function;
	}
	
	/**
	 * Deletes Function in persistence store.
	 */
	public Boolean delete(Function entity) {
		entityRepository.delete(entity);
		return true;
	}
}
