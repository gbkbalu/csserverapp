package com.balalatest.service;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.balalatest.domain.core.BaseDomain;

/**
 * Super class for all of the business services.
 * 
 * @author Shris Infotech.
 *
 */
@Service
public class GenericBusinessService {

	@Autowired
	private ApplicationContext applicationContext;

	/** Collection of all the services of type GenericBusinessService */
	private Collection<GenericBusinessService> servicesCollection;

	public GenericBusinessService() {

	}
    
	
	protected void preDelete(final BaseDomain domainObject) {

		if (servicesCollection == null || servicesCollection.isEmpty()) {
			
			Map<String, GenericBusinessService> genericBusinessServices = applicationContext
					.getBeansOfType(GenericBusinessService.class);
			if (genericBusinessServices == null) {
				return;
			}
			
			// Assign services collection
			servicesCollection = genericBusinessServices.values();
		}	
		
		if(servicesCollection == null || servicesCollection.isEmpty()) {
			return;
		}
		
		for (GenericBusinessService genericBusinessService : servicesCollection) {
			// Check if a business service is interested in delete of a domain object.
			if(genericBusinessService.isInterestedInDelete(domainObject)) {
				// Call accept delete to see if the delete is accepted by all the other business services.
				genericBusinessService.acceptDelete(domainObject);
			}
		}

	}

	protected void acceptDelete(BaseDomain domainObject) {
		// Throw exception when the domainObject under delete cannot be deleted. 
	}

	protected boolean isInterestedInDelete(BaseDomain domainObject) {
		return false;
	}
	
	protected void postDelete(final BaseDomain domainObject) {
		
		if(servicesCollection == null || servicesCollection.isEmpty()) {
				return;
		}
			
		for (GenericBusinessService genericBusinessService : servicesCollection) {
			// Check if a business service is interested in delete of a domain object.
			if(genericBusinessService.isInterestedInDelete(domainObject)) {
				// Call accept delete to see if the delete is accepted by all the other business services.
				genericBusinessService.cascade(domainObject);
			}
		}
	}


	protected void cascade(BaseDomain domainObject) {
		// Override this when you want to implement cascade the delete of type you are interested in.
	}
	
}
