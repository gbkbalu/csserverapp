package com.balalatest.service;

import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.balalatest.Application;
import com.balalatest.businessobject.Context;
import com.balalatest.businessobject.OrderBusinessLogic;
import com.balalatest.domain.Exchange;
import com.balalatest.domain.Order;
import com.balalatest.domain.OrderData;
import com.balalatest.repository.OrderRepository;

/**
 * Service object for {@link Order}
 * 
 * 
 * @see OrderRepository
 * @see OrderBusinessLogic#perform(MarketData, Context)
 * @see OrderBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
//@CacheConfig(cacheNames = "Orders")
public class OrderBusinessService extends GenericBusinessService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	OrderDataService orderDataService;
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	final OrderBusinessLogic orderBusinessLogic = new OrderBusinessLogic();
	
	
	
	/**
	 * This method is paginated version of {@link OrderService#fetchAllRecords()}
	 * 
	 * @param page
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */	
	public Page<Order> readAll(final PageRequest page) {
		final Page<Order> entities = orderDataService.readAll(page);
		orderBusinessLogic.perform(entities.getContent(), Context.GET);
		return entities;
	}
	
	public Page<Exchange> readAllExchanges(final PageRequest page) {
		final Page<Exchange> entities = orderDataService.readAllExchanges(page);
		return entities;
	}
	
	/**
	 * Fetches all the entities of type {@link Order} from the data base.
	 * 
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */
	public List<Order> fetchAllRecords() {
		final List<Order> entities = orderDataService.fetchAllRecords();
		orderBusinessLogic.perform(entities, Context.GET);
		return entities;
	}
	
	/**
	 *
     * Finds list of {@link Order} objects matching given query.
     * 
	 * @param query
	 * @return
	 */
	public List<Order> filter(Query query) {
		final List<Order> entities = orderDataService.filter(query);
		orderBusinessLogic.perform(entities, Context.FIND);
		return entities;
	}
	
	/**
	 * Returns count of {@link Order} objects matching a given query.
	 * 
	 * @param query
	 * @return count
	 */
	public Long getCount(Query query) {
		return orderDataService.getCount(query);
	}
	
	/**
	 *
     *  Returns details of {@link Order} object for a given id.
     * 
	 * @param emp
	 * @return
	 */
	public Order read(Order entity) {
		orderBusinessLogic.perform(entity, Context.GET);
		return orderDataService.read(entity);
	}
	
	/**
	 * Executes business logic on {@link Order} and persists it to the DB.
	 * 
	 * @param entity
	 *            Entity to persist in the DB
	 * @return newly created {@link Order}
	 * 
	 *  TO DO: Add @CachePut to the method if the object needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 */
	public Order create(Order entity) {
		orderBusinessLogic.perform(entity, Context.CREATE);
		return orderDataService.save(entity);
	}
	
	/**
	 * 
	 * Updates an existing {@link Order} in the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Order} is cached, update the cache as well.
	 */
	public Order update(Order entity) {
		Order existingEntity = orderDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot update Order. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot update Order. No entity with id " + entity.getId() + " exists");
		}
		orderBusinessLogic.perform(entity, Context.UPDATE);
		return orderDataService.save(entity);
	}
	
	/**
	 * 
	 * Deletes an existing {@link Order} from the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Order} is cached, delete from cache as well.
	 * Ex: @CacheEvict
	 */
	public Boolean delete(Order entity) {
		Order existingEntity = orderDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot delete Order. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot delete Order. No entity with id " + entity.getId() + " exists");
		}
		super.preDelete(entity);
		orderBusinessLogic.perform(entity, Context.DELETE);
		orderDataService.delete(existingEntity);
		super.postDelete(entity);
		return true;
	}
	
	public void saveOrderChain(OrderData orderData) {
		orderDataService.save(orderData);
	}
	
	
	
	
}
