package com.balalatest.service;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.balalatest.controller.OrderUtil;
import com.balalatest.domain.Order;
import com.balalatest.repository.OrderRepository;

@Service
public class OrderDataInitService {

  @Autowired
  private OrderRepository entityRepository;
  
  @PostConstruct
  public void saveOrderData() {
    long count = entityRepository.count();
    if(count <1000) {
      System.out.println("Saving order data list:");
      List<Order> orderDataList = OrderUtil.getOrderData();
      System.out.println("Saving order data list:" + orderDataList.size());
      entityRepository.saveAll(orderDataList);
      System.out.println("Data saved successfully" + orderDataList.size());
    }else {
      System.out.println("Data exists in the order collection");
    }
  }
}
