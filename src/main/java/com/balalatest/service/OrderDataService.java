package com.balalatest.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import com.balalatest.businessobject.Context;
import com.balalatest.businessobject.OrderBusinessLogic;
import com.balalatest.businessobject.OrderDataEnricher;
import com.balalatest.domain.Exchange;
import com.balalatest.domain.Order;
import com.balalatest.domain.OrderChain;
import com.balalatest.domain.OrderData;
import com.balalatest.repository.ExchangeRepository;
import com.balalatest.repository.OrderRepository;

/**
 * Service object for {@link Order}
 * 
 * 
 * @see OrderRepository
 * @see OrderBusinessLogic#perform(Order, Context)
 * @see OrderBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
public class OrderDataService extends GenericDataService{

	@Autowired
	private OrderRepository entityRepository;
	
	@Autowired
	private ExchangeRepository exchangeRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private OrderDataEnricher orderDataEnricher = new OrderDataEnricher();
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Saves Order to persistence store.
	 */
	public Order save(Order entity) {
		final Order order = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(order);
		return order;
	}
	
	/**
	 * Finds Order with a given id from persistence store.
	 */
	public Order findById(final String id) {
		final Order order = entityRepository.findById(id).get();
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(order);
		return order;
	}
	
	/**
	 * Reads all the Order entities from the persistence store with the size limit matching the page request.
	 */
	public Page<Order> readAll(final PageRequest page) {
		
		//project("ticker", "accountId", "clientId", "orderDataList.qty", "orderDataList.side",
        //        "orderDataList.msg", "orderDataList.orderDate", "orderDataList.id"),
		/*Criteria  criteria = new Criteria();
		//DBObject unwind = new BasicDBObject("$unwind", "$orderDataList");
		Aggregation aggrObj = newAggregation(match(criteria),
				sort(Sort.Direction.DESC, "orderDate"),
				limit(100),
				project("ticker", "accountId", "clientId", "orderDataList.qty", "orderDataList.side", "orderDataList.orderId"),
				new CustomAggregationOperation(
                        new BasicDBObject("$unwind","$orderDataList"))
		);

		AggregationResults aggrigationResult = mongoTemplate.aggregate(aggrObj, Order.class, Order.class);
		List<Order> ordersList = aggrigationResult.getMappedResults();
		
		System.out.println(ordersList.size());*/
		
		final Page<Order> entities = entityRepository.findAll(page);
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(entities);
		return entities;
	}
	
	public Page<Exchange> readAllExchanges(final PageRequest page) {
		
		final Page<Exchange> entities = exchangeRepository.findAll(page);
		return entities;
	}
	
	/**
	 * Reads all the Order entities from the persistence store.
	 */
	public List<Order> fetchAllRecords() {
		final List<Order> entities = entityRepository.findAll();
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads Order entities from the persistence store matching a give query.
	 */
	public List<Order> filter(Query query) {
		final List<Order> entities = mongoTemplate.find(query, Order.class);
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Returns count of Order entities from the persistence store.
	 */
	public Long getCount(Query query) {
		return mongoTemplate.count(query,Order.class);
	}
	
	/**
	 * Reads Order from persistence store for a given id.
	 * 
	 * @param entity
	 * @return
	 */
	public Order read(Order entity) {
		final Order order =  entityRepository.findById(entity.getId()).get();
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(order);
		return order;
	}
	
	/**
	 * Updates Order in persistence store.
	 */
	public Order update(Order entity) {
		final Order order = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		orderDataEnricher.enrich(order);
		return order;
	}
	
	/**
	 * Deletes Order in persistence store.
	 */
	public Boolean delete(Order entity) {
		entityRepository.delete(entity);
		return true;
	}

	public void save(OrderData orderData) {
	   /**
		* Client: NewSingleOrder = 35=D 11=123 P1=100 Q1=200
		* Exchange: Accept 35=8 11=123 39=0 P1=100 Q1=200
		*	Exchange: Partial 35=8 11=123 39=1 P1=98 Q1=50
		*	Exchange: Partial 35=8 11=123 39=1 P1=97 Q1=100
		*	Client: CancelReplace 35=G 11=234 41=123 P1=95 Q1=200
		*	Exchange: Accept 35=8 11=234 39=5 41=123 P1=100 Q1=200
		*	Exchange: Partial 35=8 11=234 39=1 P1=98 Q1=25
		*	Exchange: Fill 35=8 11=234 39=2 P1=96 Q1=25
    	*   summary line: Fill 35=8 11=234 39=2 P1=96 Q1=25
		*/
		
		
		String parentOrderId = orderData.getTagValues().get("tag41");
		Query query = null;
		
		if("D".equals(orderData.getTagValues().get("tag35"))) {
			// Order is a new order
			 query = new Query(Criteria.where("_id").is(orderData.getOrderId()));
		} else {
			String orderId;
			// There is a parent order id, so it must already there in the cian before.
			if(parentOrderId == null || parentOrderId.equals("0")) {
				orderId = orderData.getOrderId();
			} else {
				// There is no parent order id search for order id.
				orderId = parentOrderId;
			}
			 query = new Query(Criteria.where("orderSet").is(orderId));
		}
		
		/*db.orderchain.update(
				  {"rootOrder": "123"},
				  {$addToSet: {orderSet: "456"},$set: {"currentStatus":"s1"}},
				  { upsert: true}
				)
				  
				  db.orderchain.updateOne(
				  {"orderSet": "222"}, // compare with current order or parent order
				  {$addToSet: {orderSet: "333"},$set: {"currentStatus":"s5"}, $push: {"orderChain" : {"orderId" : "333","status" :"s5"}}},
				  { upsert: true}
				 )
		*/		  
		Update update = new Update();
		update.addToSet("orderSet", orderData.getOrderId());
		update.set("tag35", orderData.getTagValues().get("tag35"));
		update.set("tag39", orderData.getTagValues().get("tag39"));
		update.push("orderChain", orderData);
		
		mongoTemplate.findAndModify(query, update,FindAndModifyOptions.options().upsert(true), OrderChain.class);
	}
	
	
}
