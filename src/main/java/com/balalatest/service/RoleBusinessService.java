package com.balalatest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Role;
import com.balalatest.businessobject.RoleBusinessLogic;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Role}
 * 
 * 
 * @see RoleRepository
 * @see RoleBusinessLogic#perform(MarketData, Context)
 * @see RoleBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
//@CacheConfig(cacheNames = "Roles")
public class RoleBusinessService extends GenericBusinessService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	RoleDataService roleDataService;
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	final RoleBusinessLogic roleBusinessLogic = new RoleBusinessLogic();
	
	
	
	/**
	 * This method is paginated version of {@link RoleService#fetchAllRecords()}
	 * 
	 * @param page
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */	
	public Page<Role> readAll(final PageRequest page) {
		final Page<Role> entities = roleDataService.readAll(page);
		roleBusinessLogic.perform(entities.getContent(), Context.GET);
		return entities;
	}
	
	/**
	 * Fetches all the entities of type {@link Role} from the data base.
	 * 
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */
	public List<Role> fetchAllRecords() {
		final List<Role> entities = roleDataService.fetchAllRecords();
		roleBusinessLogic.perform(entities, Context.GET);
		return entities;
	}
	
	/**
	 *
     * Finds list of {@link Role} objects matching given query.
     * 
	 * @param query
	 * @return
	 */
	public List<Role> filter(Query query) {
		final List<Role> entities = roleDataService.filter(query);
		roleBusinessLogic.perform(entities, Context.FIND);
		return entities;
	}
	
	/**
	 * Returns count of {@link Role} objects matching a given query.
	 * 
	 * @param query
	 * @return count
	 */
	public Long getCount(Query query) {
		return roleDataService.getCount(query);
	}
	
	/**
	 *
     *  Returns details of {@link Role} object for a given id.
     * 
	 * @param emp
	 * @return
	 */
	public Role read(String id) {
		Role entity = new Role();
		entity.setId(id);
		roleBusinessLogic.perform(entity, Context.GET);
		return roleDataService.read(entity);
	}
	
	/**
	 * Executes business logic on {@link Role} and persists it to the DB.
	 * 
	 * @param entity
	 *            Entity to persist in the DB
	 * @return newly created {@link Role}
	 * 
	 *  TO DO: Add @CachePut to the method if the object needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 */
	public Role create(Role entity) {
		roleBusinessLogic.perform(entity, Context.CREATE);
		return roleDataService.save(entity);
	}
	
	/**
	 * 
	 * Updates an existing {@link Role} in the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Role} is cached, update the cache as well.
	 */
	public Role update(Role entity) {
		Role existingEntity = roleDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot update Role. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot update Role. No entity with id " + entity.getId() + " exists");
		}
		roleBusinessLogic.perform(entity, Context.UPDATE);
		return roleDataService.save(entity);
	}
	
	/**
	 * 
	 * Deletes an existing {@link Role} from the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link Role} is cached, delete from cache as well.
	 * Ex: @CacheEvict
	 */
	public Boolean delete(String id) {
	    Role entity = new Role();
	    entity.setId(id);
		Role existingEntity = roleDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot delete Role. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot delete Role. No entity with id " + entity.getId() + " exists");
		}
		super.preDelete(entity);
		roleBusinessLogic.perform(entity, Context.DELETE);
		roleDataService.delete(existingEntity);
		super.postDelete(entity);
		return true;
	}
}
