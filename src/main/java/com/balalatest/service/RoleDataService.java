package com.balalatest.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.Role;
import com.balalatest.repository.RoleRepository;
import com.balalatest.businessobject.RoleBusinessLogic;
import com.balalatest.businessobject.RoleDataEnricher;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link Role}
 * 
 * 
 * @see RoleRepository
 * @see RoleBusinessLogic#perform(Role, Context)
 * @see RoleBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
public class RoleDataService extends GenericDataService{

	@Autowired
	private RoleRepository entityRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private RoleDataEnricher roleDataEnricher = new RoleDataEnricher();
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Saves Role to persistence store.
	 */
	public Role save(Role entity) {
		final Role role = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(role);
		return role;
	}
	
	/**
	 * Finds Role with a given id from persistence store.
	 */
	public Role findById(final String id) {
		final Role role = entityRepository.findById(id).get();
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(role);
		return role;
	}
	
	/**
	 * Reads all the Role entities from the persistence store with the size limit matching the page request.
	 */
	public Page<Role> readAll(final PageRequest page) {
		final Page<Role> entities = entityRepository.findAll(page);
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads all the Role entities from the persistence store.
	 */
	public List<Role> fetchAllRecords() {
		final List<Role> entities = entityRepository.findAll();
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads Role entities from the persistence store matching a give query.
	 */
	public List<Role> filter(Query query) {
		final List<Role> entities = mongoTemplate.find(query, Role.class);
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Returns count of Role entities from the persistence store.
	 */
	public Long getCount(Query query) {
		return mongoTemplate.count(query,Role.class);
	}
	
	/**
	 * Reads Role from persistence store for a given id.
	 * 
	 * @param entity
	 * @return
	 */
	public Role read(Role entity) {
		final Role role =  entityRepository.findById(entity.getId()).get();
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(role);
		return role;
	}
	
	/**
	 * Updates Role in persistence store.
	 */
	public Role update(Role entity) {
		final Role role = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		roleDataEnricher.enrich(role);
		return role;
	}
	
	/**
	 * Deletes Role in persistence store.
	 */
	public Boolean delete(Role entity) {
		entityRepository.delete(entity);
		return true;
	}
}
