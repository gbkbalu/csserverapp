package com.balalatest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.User;
import com.balalatest.businessobject.UserBusinessLogic;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link User}
 * 
 * 
 * @see UserRepository
 * @see UserBusinessLogic#perform(MarketData, Context)
 * @see UserBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
//@CacheConfig(cacheNames = "Users")
public class UserBusinessService extends GenericBusinessService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserDataService userDataService;
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	final UserBusinessLogic userBusinessLogic = new UserBusinessLogic();
	
	
	
	/**
	 * This method is paginated version of {@link UserService#fetchAllRecords()}
	 * 
	 * @param page
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */	
	public Page<User> readAll(final PageRequest page) {
		final Page<User> entities = userDataService.readAll(page);
		userBusinessLogic.perform(entities.getContent(), Context.GET);
		return entities;
	}
	
	/**
	 * Fetches all the entities of type {@link User} from the data base.
	 * 
	 * @return
	 * 
	 *  TO DO: Add @Cacheable to the method if the objects needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 *  Ex: @Cacheable(unless = "#result != null and #result.size() == 0")
	 */
	public List<User> fetchAllRecords() {
		final List<User> entities = userDataService.fetchAllRecords();
		userBusinessLogic.perform(entities, Context.GET);
		return entities;
	}
	
	/**
	 *
     * Finds list of {@link User} objects matching given query.
     * 
	 * @param query
	 * @return
	 */
	public List<User> filter(Query query) {
		final List<User> entities = userDataService.filter(query);
		userBusinessLogic.perform(entities, Context.FIND);
		return entities;
	}
	
	/**
	 * Returns count of {@link User} objects matching a given query.
	 * 
	 * @param query
	 * @return count
	 */
	public Long getCount(Query query) {
		return userDataService.getCount(query);
	}
	
	/**
	 *
     *  Returns details of {@link User} object for a given id.
     * 
	 * @param emp
	 * @return
	 */
	public User read(String id) {
		User entity = new User();
		entity.setId(id);
		userBusinessLogic.perform(entity, Context.GET);
		return userDataService.read(entity);
	}
	
	/**
	 * Executes business logic on {@link User} and persists it to the DB.
	 * 
	 * @param entity
	 *            Entity to persist in the DB
	 * @return newly created {@link User}
	 * 
	 *  TO DO: Add @CachePut to the method if the object needs to be
	 *  cached. This needs to work in conjunction with enabling cache. Check {@link Application} for cache configuration.
	 */
	public User create(User entity) {
		userBusinessLogic.perform(entity, Context.CREATE);
		return userDataService.save(entity);
	}
	
	/**
	 * 
	 * Updates an existing {@link User} in the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link User} is cached, update the cache as well.
	 */
	public User update(User entity) {
		User existingEntity = userDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot update User. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot update User. No entity with id " + entity.getId() + " exists");
		}
		userBusinessLogic.perform(entity, Context.UPDATE);
		return userDataService.save(entity);
	}
	
	/**
	 * 
	 * Deletes an existing {@link User} from the database.
	 * 
	 * 
	 * @param entity
	 * @return
	 * 
	 * TO DO: If the {@link User} is cached, delete from cache as well.
	 * Ex: @CacheEvict
	 */
	public Boolean delete(String id) {
	    User entity = new User();
	    entity.setId(id);
		User existingEntity = userDataService.findById(entity.getId());

		if (existingEntity == null) {
			logger.error("Cannot delete User. No entity with id " + entity.getId() + " exists");
			throw new IllegalArgumentException("Cannot delete User. No entity with id " + entity.getId() + " exists");
		}
		super.preDelete(entity);
		userBusinessLogic.perform(entity, Context.DELETE);
		userDataService.delete(existingEntity);
		super.postDelete(entity);
		return true;
	}
}
