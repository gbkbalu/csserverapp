package com.balalatest.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.context.MessageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.balalatest.domain.User;
import com.balalatest.repository.UserRepository;
import com.balalatest.businessobject.UserBusinessLogic;
import com.balalatest.businessobject.UserDataEnricher;
import com.balalatest.businessobject.Context;

/**
 * Service object for {@link User}
 * 
 * 
 * @see UserRepository
 * @see UserBusinessLogic#perform(User, Context)
 * @see UserBusinessLogic#perform(java.util.Collection, Context)
 * 
 * @author Shris Infotech
 */
@Service
public class UserDataService extends GenericDataService{

	@Autowired
	private UserRepository entityRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private UserDataEnricher userDataEnricher = new UserDataEnricher();
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Saves User to persistence store.
	 */
	public User save(User entity) {
		final User user = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(user);
		return user;
	}
	
	/**
	 * Finds User with a given id from persistence store.
	 */
	public User findById(final String id) {
		final User user = entityRepository.findById(id).get();
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(user);
		return user;
	}
	
	/**
	 * Reads all the User entities from the persistence store with the size limit matching the page request.
	 */
	public Page<User> readAll(final PageRequest page) {
		final Page<User> entities = entityRepository.findAll(page);
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads all the User entities from the persistence store.
	 */
	public List<User> fetchAllRecords() {
		final List<User> entities = entityRepository.findAll();
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Reads User entities from the persistence store matching a give query.
	 */
	public List<User> filter(Query query) {
		final List<User> entities = mongoTemplate.find(query, User.class);
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(entities);
		return entities;
	}
	
	/**
	 * Returns count of User entities from the persistence store.
	 */
	public Long getCount(Query query) {
		return mongoTemplate.count(query,User.class);
	}
	
	/**
	 * Reads User from persistence store for a given id.
	 * 
	 * @param entity
	 * @return
	 */
	public User read(User entity) {
		final User user =  entityRepository.findById(entity.getId()).get();
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(user);
		return user;
	}
	
	/**
	 * Updates User in persistence store.
	 */
	public User update(User entity) {
		final User user = entityRepository.save(entity);
		//If needed, enrich data before returning to the business service.
		userDataEnricher.enrich(user);
		return user;
	}
	
	/**
	 * Deletes User in persistence store.
	 */
	public Boolean delete(User entity) {
		entityRepository.delete(entity);
		return true;
	}
}
