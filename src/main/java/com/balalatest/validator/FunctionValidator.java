package com.balalatest.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Component;
import com.balalatest.domain.Function;

/**
 * Validator for {@link Function} domain object.
 *
 * @author Shris Infotech
 */
 @Component
public class FunctionValidator extends GenericValidator {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	* Use resource bundles for error messages.
	*
	* Sample usage:
	* messageSource.getMessage("key",null,Locale.US);
	* This will find message from messages.properties file in class path for the locale passed.
	*/
	
	public boolean supports(Class<?> clazz) {
		return true;
	}

	public void validate(Object target, Errors errors) {
		if(!(target instanceof Function)) {
			logger.error("Cannot validate Function. Class cast error.");
			return;
		}
		super.validate(target, errors);
		final Function function = (Function)target;
		// Validations here
		//----------------------------
		// Example 1
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required", new Object[] { "Ticker name" },"Required");
			
		// Example 2
		// errors.reject("invalid.name", messageSource.getMessage("invalid.name", null, Locale.US));
		// Here invalid.name as a key there should a value like - Invalid value for name in messages.properties
	}
}
