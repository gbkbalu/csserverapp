package com.balalatest.validator;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.balalatest.common.QueryUtil;
import com.balalatest.domain.core.BaseDomain;
import com.balalatest.domain.core.Operator;
import com.balalatest.domain.core.annotation.Required;
import com.balalatest.domain.core.annotation.RequiredWhen;
import com.balalatest.domain.core.annotation.Unique;
import com.balalatest.domain.core.annotation.UniqueFields;

@Component
public class GenericValidator implements Validator {

	@Autowired
	QueryUtil queryUtil;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	private MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof BaseDomain) {
			// Validate required fields
			validateRequiredFields((BaseDomain) target, errors);
			// Validate required fields
			try {
				validateConditionallyRequiredFields((BaseDomain) target, errors);
			} catch (Exception e) {
				// Do nothing
				e.printStackTrace();
			}
			// Validate unique fields
			validateUniqueFields((BaseDomain) target, errors);
		}
	}

	private void validateConditionallyRequiredFields(BaseDomain baseDomain, Errors errors) throws Exception {
		for (Field field : baseDomain.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(RequiredWhen.class)) {
				RequiredWhen annotation = field.getAnnotation(RequiredWhen.class);
				if (baseDomain.getProperty(field.getName()) == null
						&& isRequiredWhenConditionMet(baseDomain, annotation)) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, field.getName(), "Required.field",
							new Object[] { field.getName() }, "Required");
				}
			}
		}

	}

	private boolean isRequiredWhenConditionMet(BaseDomain baseDomain, RequiredWhen annotation) throws Exception {
		
		Object filedValue = baseDomain.getProperty(annotation.field());
		String annotationValue = annotation.value();
		Operator operator = annotation.operator(); 
		if(filedValue instanceof  Number) {
			
			Number left = (Number) filedValue;
			Number right = Double.valueOf(annotationValue);
			
			if(Operator.Equals.equals(operator)) {
				return left.equals(right);
			} else if(Operator.NotEquals.equals(operator)){
				return !left.equals(right);
			} else if(Operator.GreatherThan.equals(operator)) {
				return left.doubleValue() > right.doubleValue();
			} else if(Operator.LessThan.equals(operator)) {
				return left.doubleValue() < right.doubleValue();
			} else if(Operator.Empty.equals(operator)) {
				return  filedValue == null;
			} else if(Operator.NotEmpty.equals(operator)) {
				return  filedValue != null;
			}
			
		} else if(filedValue instanceof String) {
			
			String left = (String) filedValue;
			String right = annotationValue;
			
			if(Operator.Equals.equals(operator)) {
				return left.equals(right);
			} else if(Operator.NotEquals.equals(operator)){
				return !left.equals(right);
			} else if(Operator.GreatherThan.equals(operator)) {
				return left.compareTo(right) > 0;
			} else if(Operator.LessThan.equals(operator)) {
				return left.compareTo(right) < 0;
			} else if(Operator.Empty.equals(operator)) {
				return  (filedValue == null || "".equals(filedValue));
			} else if(Operator.NotEmpty.equals(operator)) {
				return  (filedValue != null && !"".equals(filedValue));
			}
			
			
		} if(filedValue instanceof  Date) {
			
			Date left = (Date) filedValue;
			Date right = new SimpleDateFormat().parse(annotationValue);
			
			if(Operator.Equals.equals(operator)) {
				return left.equals(right);
			} else if(Operator.NotEquals.equals(operator)){
				return !left.equals(right);
			} else if(Operator.GreatherThan.equals(operator)) {
				return left.compareTo(right) > 0;
			} else if(Operator.LessThan.equals(operator)) {
				return left.compareTo(right) < 0;
			} else if(Operator.Empty.equals(operator)) {
				return  filedValue == null;
			} else if(Operator.NotEmpty.equals(operator)) {
				return  filedValue != null;
			}
			
		} if(filedValue!= null && filedValue.getClass().isEnum()) {
			String left = filedValue.toString();
			String right = annotationValue;
			
			if(Operator.Equals.equals(operator)) {
				return left.equals(right);
			} else if(Operator.NotEquals.equals(operator)){
				return !left.equals(right);
			} else if(Operator.GreatherThan.equals(operator)) {
				return left.compareTo(right) > 0;
			} else if(Operator.LessThan.equals(operator)) {
				return left.compareTo(right) < 0;
			} else if(Operator.Empty.equals(operator)) {
				return  filedValue == null;
			} else if(Operator.NotEmpty.equals(operator)) {
				return  filedValue != null;
			}
		}
		
		return false;
	}

	/**
	 * Validates required fields.
	 * 
	 * @param baseDomain
	 * @param errors
	 */
	public void validateRequiredFields(BaseDomain baseDomain, Errors errors) {

		List<String> requiredFields = getRequiredFiledNames(baseDomain);
		// If there are no fields annotated with @Required annotation, just
		// return.
		if (requiredFields == null || requiredFields.isEmpty()) {
			// Do nothing
			return;
		}

		for (String fieldName : requiredFields) {

			try {
				if (baseDomain.getProperty(fieldName) == null) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, fieldName, "Required.field",
							new Object[] { fieldName }, "Required");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Validates unique fields
	 * 
	 * @param baseDomain
	 * @param errors
	 */
	public void validateUniqueFields(BaseDomain baseDomain, Errors errors) {

		Unique unique = baseDomain.getClass().getAnnotation(Unique.class);
		if (unique == null) {
			return;
		}

		UniqueFields[] uniqueFileds = unique.unique();
		Map<UniqueFields, Query> queriesMap = queryUtil.createQueryFromUniqueAnnotation(baseDomain, uniqueFileds);

		if (queriesMap == null) {
			return;
		}

		for (UniqueFields key : queriesMap.keySet()) {
			Query query = queriesMap.get(key);
			BaseDomain domainObject = mongoTemplate.findOne(query, baseDomain.getClass());
			// Already a value for the given fields exist. Throw an error.
			// Throw an error if a domain object already exists and in case of update it should not be same as the cirrent one.
			if (domainObject != null && !domainObject.getId().equals(baseDomain.getId())) {
				errors.reject("invalid.name", messageSource.getMessage("Unique.fileds",
						new String[] { StringUtils.join(key.fields(), ",") }, Locale.US));
			}
		}
	}

	/**
	 * Helper method to find fields marked with Required annotation.
	 * 
	 * @param baseDomain
	 * @return
	 */
	private List<String> getRequiredFiledNames(BaseDomain baseDomain) {

		List<String> fieldNames = new ArrayList<String>();
		for (Field field : baseDomain.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(Required.class)) {
				fieldNames.add(field.getName());
			}
		}

		return fieldNames;
	}

}
